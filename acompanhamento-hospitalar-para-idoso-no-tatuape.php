<?php
$title       = "Acompanhamento Hospitalar para Idoso no Tatuapé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nós contamos com os melhores profissionais em nosso serviço de Acompanhamento Hospitalar para Idoso no Tatuapé, para que a família possa depositar total confiança em nosso trabalho e para que também, possamos atender a todas as necessidades de nossos pacientes. Fazemos todo o acompanhamento necessário, para que o idoso tenha constante evoluções no que for necessário durante o acompanhamento.</p>
<p>A empresa Onix Gestão Do Cuidado é destaque entre as principais empresas do ramo de Cuidado ao Idoso, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Acompanhamento Hospitalar para Idoso no Tatuapé do mercado. Ainda, possui facilidade com Serviço de Cuidadores de Idosos, Empresa de Cuidadores de Idosos Domiciliar, Alzheimer Cuidados de Enfermagem, Cuidador de Idosos com Alzheimer e Serviço de Acompanhamento de Idosos mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>