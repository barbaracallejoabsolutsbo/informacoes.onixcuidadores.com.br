<?php
$title       = "Cuidador de Idosos para Dar Banho na Cidade Patriarca";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Além do serviço de Cuidador de Idosos para Dar Banho na Cidade Patriarca, nós fornecemos serviços similares aos nossos clientes, para que possamos auxilia-los no que for necessário. Nós fornecemos nossos serviços em ambientes como, domicílio, hospitais, casa de repouso e clínicas. Portanto, nos apresente a opção que for mais viável a você, para que possamos te entregar nossos melhores trabalhos.</p>
<p>Trabalhando para garantir Cuidador de Idosos para Dar Banho na Cidade Patriarca de maior qualidade no segmento de Cuidado ao Idoso, contar com a Onix Gestão Do Cuidado que, proporciona, também, Cuidador de Idosos em Casa, Cuidador de Idosos Hospitalar, Serviço de Acompanhamento de Idosos, Acompanhante de Idosos em Hospital e Gerontologia Cuidado ao Idoso com a mesma excelência e dedicação se torna a melhor decisão para você. Se destacando de forma positiva das demais empresas, nós contamos com profissionais amplamente capacitados para um atendimento de sucesso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>