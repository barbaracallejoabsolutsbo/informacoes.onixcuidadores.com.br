<?php
$title       = "Serviço de Acompanhamento de Idosos em Pari";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Todos os profissionais que realizam o Serviço de Acompanhamento de Idosos em Pari da Onix, possuem grandes experiências nesse ramo, para que estejam aptos a exercerem suas funções dentro de nossa empresa. Além de que são diplomatas e por consequência possuem um conhecimentos vasto também no mesmo. Estamos em constante evolução para que possamos atender cada vez mais as expectativas de nossos pacientes.</p>
<p>Com uma ampla atuação no segmento, a Onix Gestão Do Cuidado oferece o melhor quando falamos de Serviço de Acompanhamento de Idosos em Pari proporcionando aos seus clientes a máxima qualidade e desempenho em Avaliação Gerontológica Preço, Cuidador Hospitalar Preço, Cuidador de Idosos para Dar Banho, Acompanhamento de Idosos em Hospitais e Cuidador de Idosos 3 Vezes por Semana, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de Cuidado ao Idoso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>