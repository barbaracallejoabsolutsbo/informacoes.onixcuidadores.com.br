<?php
$title       = "Serviços de Transporte para Idosos em Santos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Onix realiza Serviços de Transporte para Idosos em Santos, para facilitar a locomoção dos mesmos onde precisarem. Nós temos todos os recursos necessários para atendermos e correspondermos a todas as necessidades de nossos pacientes. Estamos sempre absorvendo e aplicando novas técnicas em nossos métodos utilizados em todos os atendimentos. Portanto, não deixe de garantir o seu atendimento.</p>
<p>Nós da Onix Gestão Do Cuidado estamos entre as principais empresas qualificadas no ramo de Cuidado ao Idoso. Temos como principal missão realizar uma ótima assessoria tanto em Serviços de Transporte para Idosos em Santos, quanto à Cuidador de Idosos para Dar Banho, Acompanhamento de Idosos em Hospitais, Cuidador de Idosos com Alzheimer, Cuidador de Idosos Hospitalar e Empresa de Acompanhante de Idosos, uma vez que, contamos com profissionais qualificados e prontos para realizarem um ótimo atendimento. Entre em contato conosco e tenha a satisfação que busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>