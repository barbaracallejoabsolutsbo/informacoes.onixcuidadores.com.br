<?php
$title       = "Cuidador de Idosos 3 Vezes por Semana na Vila Gustavo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O profissional Cuidador de Idosos 3 Vezes por Semana na Vila Gustavo, a cada acompanhamento, realiza e entrega relatórios diários para que a família e responsáveis do idosos, possa analisar de perto, os resultados que os nossos trabalhos estão trazendo à vida dos nossos pacientes. Através de nossos site, você poderá falar diretamente com um de nossos especialistas e tirar todas as dúvidas que possuir.</p>
<p>A Onix Gestão Do Cuidado, como uma empresa em constante desenvolvimento quando se trata do mercado de Cuidado ao Idoso visa trazer o melhor resultado em Cuidador de Idosos 3 Vezes por Semana na Vila Gustavo para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Cuidar de Idosos a Noite, Empresa de Acompanhante de Idosos, Cuidadora de Idosos Particular, Acompanhamento de Idosos em Hospitais e Cuidadora de Idosos Preço para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>