<?php
$title       = "Acompanhamento de idosos em hospitais";
$description = "Consulte o quanto antes os profissionais da Onix, para tirar qualquer dúvida que possuir perante ao nosso acompanhamento de idosos em hospitais.";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">

        <div class="titulo-personalizado">
            <div class="container">
                <div class="col-md-8">
                    <h1 class="main-title"><?php echo $h1; ?></h1>
                </div>
                <div class="col-md-4">
                    <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
                </div>
            </div>
        </div>


        <section class="container">         
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O nosso serviço de acompanhamento de idosos em hospitais, é essencial para aqueles que se encontram numa situação vulnerável, internados em hospitais, clínicas de retaguarda ou longa permanência e casas de repouso. Onde os mesmos necessitam de uma atenção individual, independente da hora do dia que for. Os nossos profissionais possuem anos de experiência em acompanhamento de idosos em hospitais e diversos serviços correlacionados, portanto, os mesmos são extremamente aptos para estar juntos dos idosos, auxiliando-os no que for necessário, pata melhorarem seus bem-estar e suas qualidades de vida. O acompanhamento de idosos em hospitais, serve para que também nossos especialistas, acompanhem de perto, cada procedimento que for feito dentro da instituição, para garantir que os idosos tenham suas necessidades básicas, durante seus períodos na mesma. Elaboramos relatórios diários, para que os familiares e responsáveis, possam fazer o acompanhamento conforme achem necessário. Fora os benefícios já citados, o acompanhamento de idosos em hospitais da onix, te levará mais conforto e segurança, pois você saberá que os mesmos estão sendo bem tratados pelos nossos profissionais. Estamos sempre estudando novos métodos e novas técnicas em nossos serviços, para que sejamos cada vez mais referência nesse ramo, pela nossa qualidade. Aprimoramos cada vez mais nossos recursos, para entregarmos um incrível serviço, para todos aqueles que vêm a nos consultar. Fazemos um atendimento exclusivo e personalizado aos nossos pacientes, para que os mesmos tenham todas as suas necessidades correspondidas pelos nossos funcionários; e também para que sempre que você precisar desse ou demais serviços que temos disponibilizados em nosso site, nós sejamos sua primeira opção. Consulte um de nossos profissionais o quanto antes, para que quem você ama tenham experiências incríveis com o nosso zelo e atenção.</p>

                    <h2>Mais detalhes sobre nosso acompanhamento de idosos em hospitais</h2>
                    <p><br />Nós avaliamos e auxiliamos idosos em atividades diárias em nosso acompanhamento de idosos em hospitais e demais instituições, até mesmo para passarmos um relatório correto à família. Fora que os mesmos terão uma melhora em suas qualidades de vida, com a nossa equipe extremamente qualificada. Nos adaptamos as necessidades de cada cliente, portanto não hesite em nos consultar.</p>

                    <h3>A melhor opção de acompanhamento de idosos em hospitais</h3>
                    <p><br />Você pode falar com um de nossos especialistas através do nosso site mesmo, onde eles te auxiliarão em qualquer dúvida que possuir perante ao nosso acompanhamento de idosos em hospitais. Se preferir, nos faça uma visita. Estamos localizados em São Paulo.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>