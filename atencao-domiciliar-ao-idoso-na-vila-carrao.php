<?php
$title       = "Atenção Domiciliar ao Idoso na Vila Carrão";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Durante a nossa Atenção Domiciliar ao Idoso na Vila Carrão, nossos profissionais, gerontólogos e/ou enfermeiros, realizam atividades diárias com os mesmos, para que através de nossos trabalhos, possamos melhorar a qualidade de vida de nosso paciente, no que nos for solicitado. Realizamos também relatórios ao fim de cada acompanhamento, para analisar as constantes evoluções do idoso</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Cuidado ao Idoso, a Onix Gestão Do Cuidado ganha destaque por ser confiável e idônea quando falamos não só de Atenção Domiciliar ao Idoso na Vila Carrão, mas também quando o assim é Agência de Home Care Cuidador, Gerontologia Cuidado ao Idoso, Acompanhamento Hospitalar para Idoso, Cuidador de Idosos Hospitalar e Serviços de Transporte para Idosos. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>