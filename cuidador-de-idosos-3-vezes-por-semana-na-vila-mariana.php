<?php
$title       = "Cuidador de Idosos 3 Vezes por Semana na Vila Mariana";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O profissional Cuidador de Idosos 3 Vezes por Semana na Vila Mariana, a cada acompanhamento, realiza e entrega relatórios diários para que a família e responsáveis do idosos, possa analisar de perto, os resultados que os nossos trabalhos estão trazendo à vida dos nossos pacientes. Através de nossos site, você poderá falar diretamente com um de nossos especialistas e tirar todas as dúvidas que possuir.</p>
<p>Com uma alta credibilidade no mercado de Cuidado ao Idoso, proporcionando com qualidade, viabilidade e custo x benefício em Cuidador de Idosos 3 Vezes por Semana na Vila Mariana, a empresa Onix Gestão Do Cuidado vem crescendo e mostrando seu potencial através, também de Empresa de Cuidadores de Idosos, Diária de um Cuidador de Idoso, Quanto Custa Cuidadores de Idosos, Acompanhamento de Idosos em Hospitais e Serviços de Transporte para Idosos, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>