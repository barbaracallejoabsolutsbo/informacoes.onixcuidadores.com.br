<?php
    $title       = "Acompanhante de idosos";
    $description = "Garanta já o serviço de acompanhante de idosos da Onix. Nossos profissionais são diariamente treinados para te entregarem seus melhores serviços. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O nosso serviço de acompanhante de idosos é viável e necessário para você que deseja levar um atendimento específico e personalizados para quem necessita dos mesmos. Nós fornecemos os nossos serviços de acompanhante de idosos em domicílios, casa de Repouso, clínicas e hospitais; portanto independente para onde você deseja esse e demais serviços da onix, os mesmos são aptos para você. Para que você tenha uma ótima experiência com o cliente e paciente, nosso acompanhante de idosos é essencial. Nossos profissionais aprimoram suas habilidades a cada dia, para que sejamos cada vez mais referência com o nosso acompanhante de idosos e para que também, estejamos aptos para qualquer tipo de solicitação. Em todos os nossos serviços, nossos especialistas auxiliam os idosos em atividades diárias, para que os mesmos possam se desenvolver cada vez mais e também, para que possa haver uma melhoria em suas qualidades de vida. Um dos nossos objetivos é levarmos o máximo de conforto e segurança para os familiares/responsáveis e idosos; e para isso, nós disponibilizamos a troca de nossos profissionais caso haja uma falta de adaptação e também, uma possibilidade de alteração no horário de atendimento. Pois nós sabemos que os nossos serviços só levarão benefícios a você e fazemos o que for para que você possa obtê-los. Na onix, há somente profissionais capacitados e diplomatas, como gerontólogos e enfermeiros, para que possam exercem suas funções dentro de nossa empresa com grande êxito. Todavia, não perca essa oportunidade de poder vivenciar a experiência de usufruir de nossos serviços e ainda melhorando sua qualidade de vida. Estamos em constante estudo, para que nossos métodos e recursos sejam cada vesz mais atualizados e os melhores desse mercado.</p>

<h2>Mais detalhes sobre nossos acompanhante de idosos</h2>
<p>Nós entregamos nosso melhor atendimento, não só com nosso acompanhante de idosos, mas em todos os nossos serviços, para que sempre que você necessitar do mesmo, ou de outros que temos disponíveis em nosso site, você já saiba que somos o melhor lugar para se recorrer. Não hesite em nos consultar, pois como já citado, nós nos moldamos para que você tenha qualquer um de nossos serviços aonde e na hora que desejar.</p>

<h3>O melhor lugar para adquirir acompanhante de idosos</h3>
<p>Agende uma avaliação conosco, perante ao nosso serviço de acompanhante de idosos, para que possamos personalizar o seu atendimento e então, ter todas as suas necessidades correspondidas. Estamos aguardamos ansiosamente pelo seu contato.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>