<?php
$title       = "Gerontologia Cuidado ao Idoso em São Caetano do Sul";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nosso serviço de Gerontologia Cuidado ao Idoso em São Caetano do Sul, nossos profissionais realizam atividades diárias com os nossos pacientes, para que os mesmos tenham constante evoluções em seus quadros e possam realizar seus afazeres diários sem nenhum empecilho o quanto antes. Nos adaptamos a rotina de nossos pacientes, moldando sempre o que for necessário para garantir o bem-estar dos mesmos.</p>
<p>Empresa pioneira no mercado de Cuidado ao Idoso, a Onix Gestão Do Cuidado é qualificada e experiente quando o assunto são Treinamentos para Cuidadores de Idosos, Diária de um Cuidador de Idoso, Cuidador de Idosos com Fratura de Fêmur, Cuidador de Idosos Fim de Semana e Cuidador de Idosos com Alzheimer. Aqui você encontra soluções personalizadas para o que necessita e tem acesso ao que há de melhor e mais moderno para Gerontologia Cuidado ao Idoso em São Caetano do Sul sempre com muita eficiência e qualidade para garantir a sua satisfação e sua fidelidade conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>