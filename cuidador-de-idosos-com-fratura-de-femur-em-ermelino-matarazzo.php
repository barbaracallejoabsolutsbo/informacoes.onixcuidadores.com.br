<?php
$title       = "Cuidador de Idosos com Fratura de Fêmur em Ermelino Matarazzo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>É necessário consultar um serviço de Cuidador de Idosos com Fratura de Fêmur em Ermelino Matarazzo, pois os profissionais qualificados prestarão todos os serviços que são necessários para que haja uma constante melhora e também, para que o idoso consiga realizar suas atividades diárias sem nenhum empecilho. Visamos melhorar a qualidade de vida de nossos pacientes, para que os mesmos sejam cada vez mais independentes.
</p>
<p>Empresa pioneira no mercado de Cuidado ao Idoso, a Onix Gestão Do Cuidado é qualificada e experiente quando o assunto são Serviço de Cuidadores de Idosos, Cuidador de Idosos Fim de Semana, Quanto Custa Cuidador 24 horas, Serviços de Transporte para Idosos e Avaliação Gerontológica Preço. Aqui você encontra soluções personalizadas para o que necessita e tem acesso ao que há de melhor e mais moderno para Cuidador de Idosos com Fratura de Fêmur em Ermelino Matarazzo sempre com muita eficiência e qualidade para garantir a sua satisfação e sua fidelidade conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>