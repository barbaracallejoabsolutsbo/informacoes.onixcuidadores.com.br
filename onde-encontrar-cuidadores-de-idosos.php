<?php
$title       = "Onde Encontrar Cuidadores de Idosos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Para você que está em busca de onde encontrar cuidadores de idosos experientes e com grandes estudos e conhecimentos, a Onix é o lugar ideal. Estamos há longos anos atuando nesse ramo, onde a cada um, aprimoramos nossas habilidades e técnicas para entregarmos um serviço cada vez melhor. Portanto, não deixe de nos consultar para ter de perto os nossos serviços de qualidade.</p><h2>Saiba onde encontrar cuidadores de idosos</h2><p>´Desde o seu primeiro contato conosco, você verá que os nossos atendimentos são exclusivo, pois você se torna nossa máxima prioridade. E também, você já terá ciência que somos o melhor lugar de onde encontrar cuidadores de idosos. Queremos fazer com que você e nosso paciente, tenham a melhor experiência conosco. Não perca essa oportunidade única.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>