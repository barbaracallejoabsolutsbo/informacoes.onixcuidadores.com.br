<?php

        // Principais Dados do Cliente
    $nome_empresa = "Onix Cuidadores";
    $emailContato = "contato@onixgestao.com.br ";

    // Parâmetros de Unidade
    $unidades = array(
    1 => array(
        "nome" => "Onix Cuidadoes",
        "rua" => "Rua Alvarenga, 700",
        "bairro" => "Butantã",
        "cidade" => "São Paulo",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "05509-000",
        "latitude_longitude" => "", // Consultar no maps.google.com
        "ddd" => "11",
        "telefone" => "3214-1652",
        "telefone2" => "93027-0237",
        "whatsapp" => "98430-0237",
        "whatsapp-link" => "https://api.whatsapp.com/send/?phone=5511984300237&text=Olá%2C+gostaria+de+solicitar+um+serviço&app_absent=0",
        "link_maps" => "https://www.google.com.br/maps/place/R.+Alvarenga,+700+-+Butantã,+São+Paulo+-+SP,+05509-000/@-23.5756263,-46.7124467,17z/data=!3m1!4b1!4m5!3m4!1s0x94ce56ffc037418b:0xa6490ffe37a3c781!8m2!3d-23.5756312!4d-46.7102527?hl=pt-BR&authuser=0" // Incorporar link do maps.google.com
    ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/informacoes.onixcuidadores.com.br/",
        // URL online
        "https://www.informacoes.onixcuidadores.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
	
    // Parâmetros para Formulário de Contato
    $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
    // $smtp_contato            = "111.111.111.111";
    // $email_remetente         = "formulario@temporario-clientes.com.br";
    // $senha_remetente         = "4567FGHJK";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
        "Acompanhamento de idosos em hospitais",
        "Acompanhamento gerontológico",
        "Acompanhamento hospitalar",
        "Acompanhamento hospitalar para idoso",
        "Acompanhante de idosos",
        "Acompanhante de idosos em hospital",
        "Agência de home care cuidador",
        "Alzheimer cuidados de enfermagem",
        "Atenção domiciliar",
        "Avaliação gerontológica",
        "Cuidador de idosos 3 vezes por semana",
        "Cuidador de idosos com Alzheimer",
        "Cuidador de idosos com fratura de fêmur",
        "Cuidador de idosos fim de semana",
        "Cuidador de idosos hospitalar",
        "Cuidador de idosos para dar banho",
        "Cuidador em casa",
        "Cuidador hospitalar preço",
        "Cuidadora de idosos particular",
        "Cuidadora de idosos preço",
        "Cuidadores de idosos",
        "Cuidadores de idosos em SP",
        "Cuidados para idosos",
        "Cuidar de idosos a noite",
        "Cuidar de idosos fim de semana",
        "Empresa de cuidador de idosos",
        "Empresa de cuidadores de idosos domiciliar",
        "Empresa de cuidadores de idosos em São Paulo",
        "Empresa terceirizada de cuidadores de idosos",
        "Equipe multiprofissional",
        "Gerontologia cuidado ao idoso",
        "Home care cuidador de idosos",
        "Home care idosos",
        "Onde encontrar cuidadores de idosos em SP",
        "Preço de cuidadores de idosos",
        "Quanto custa cuidador 24 horas",
        "Serviço de acompanhamento de idosos",
        "Serviço de cuidadores de idosos",
        "Serviços de transporte para idosos",
        "Treinamentos para cuidadores em SP",

    );
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */