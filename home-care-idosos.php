<?php
    $title       = "Home care idosos";
    $description = "A Onix, fornece o serviço de home care idosos, para que possamos ajudar os mesmos no que for necessários para que tenham uma qualidade de vida melhor.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O serviço de home care idosos ,é extremamente importante para que os idosos tenham um devido cuidado necessário com profissionais experientes e qualificados nesse ramo. Durante o home care idosos, nossos profissionais executam atividades para os nossos pacientes, auxiliando-os no que for necessário para que os mesmos tenham cada vez mais suas independências no dia a dia. São feitos também, relatórios diários para que a família e responsáveis possam fazer um acompanhamento mais detalhado de como o nosso serviço de Home care idosos está sendo aplicado. Queremos sempre fazer com que os nossos pacientes tenham o máximo de conforto possível ao presenciar qualquer um de nossos atendimentos; e para isso, nós entregamos nossos melhores trabalhos desde seu primeiro contato conosco, até mesmo para que quando você precisar de serviços como home care idosos e demais outros que disponibilizamos em nosso site, nós sejamos uma de suas primeiras opções. Priorizamos sempre que as vontades e prioridades de nossos pacientes sejam correspondidas e para isso, mudamos o que for necessário em nossos serviços. Como por exemplo, nós disponibilizamos a troca de nossos funcionários durante o nosso acompanhamento, caso haja uma falta de adaptação dos nossos pacientes para com os mesmos. Nós também realizamos um acompanhamento integral ou suporte 24h00, conforme forem as necessidades de cada paciente. Vale lembrar, que esse e demais serviços que fornecemos, podem ser prestados em domicílios, casas de repouso, hospitais, clínicas e demais outros lugares. Portanto, consulte um de nossos profissionais o quanto antes para obter a opção mais viável a você. Nós contamos com profissionais gerontólogos, enfermeiros E demais outros da área da saúde para que sejamos sempre aptos para atender a qualquer tipo de solicitação. Garantimos que ao consultar um de nossos serviços, não haverá com que se preocupar.</p>

<h2>Mais detalhes sobre o nosso serviço de home care idosos</h2>
<p>Sendo de uma forma emergencial ou planejada, o serviço de Home care idosos não deixa de ser importante, tanto para o paciente, quanto para a família e responsáveis. Através disso, nós fazemos a questão de deixar esse e demais serviços com preços acessíveis, para que a qualquer momento em que você necessitar de algum deles, os mesmos estejam ao seu alcance.</p>
<h3>A melhor opção de home care idosos</h3>
<p>Entregamos sempre nossos melhores trabalhos, para que os nossos clientes e pacientes tenham as melhores experiências ao consultar nosso serviço de home care idosos, ou qualquer outro que temos disponíveis. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>