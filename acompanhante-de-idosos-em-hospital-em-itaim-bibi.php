<?php
$title       = "Acompanhante de Idosos em Hospital em Itaim Bibi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Além das atividades fornecidas pelo nosso Acompanhante de Idosos em Hospital em Itaim Bibi, os mesmos realizam relatórios diários para que a família e responsáveis possam também, acompanhar de perto de como os nosso serviços estão sendo aplicados, juntamente com os resultados que serão apresentados. Agende uma avaliação com um de nossos especialistas através de nosso site ou aplicativo.</p>
<p>Se destacando entre as mais competentes empresas do segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado vem proporcionando com total empenho seja em Acompanhante de Idosos em Hospital em Itaim Bibi quanto em Atenção Domiciliar ao Idoso, Equipe Multiprofissional de Atenção Domiciliar, Home Care Cuidador de Idosos, Cuidador de Idosos para Dar Banho e Cuidar de Idosos em Casas Particulares, com o foco em agregando qualidade e excelência em seu atendimento nossa empresa busca a constante melhoria e desenvolvimento de seus colaboradores de forma a garantir o melhor para quem nos busca.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>