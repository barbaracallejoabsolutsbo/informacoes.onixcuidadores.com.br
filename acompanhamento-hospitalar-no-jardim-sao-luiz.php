<?php
$title       = "Acompanhamento Hospitalar no Jardim São Luiz";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Durante o nosso Acompanhamento Hospitalar no Jardim São Luiz, os nossos profissionais prestam todos os serviços necessários para que as necessidades de nossos pacientes sejam sempre correspondidas. Queremos fazer com que o idoso se sinta o mais confortável possível durante o acompanhamento  e para isso, nós fazemos o que for necessário, como por exemplo, a troca de funcionários por falta de adaptação com os mesmos.</p>
<p>Especialista no segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Acompanhamento Hospitalar no Jardim São Luiz. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Acompanhamento Hospitalar para Idoso, Quanto Custa Cuidadores de Idosos, Empresa Terceirizada de Cuidadores de Idosos, Cuidadora de Idosos Particular e Equipe Multiprofissional de Atenção Domiciliar e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>