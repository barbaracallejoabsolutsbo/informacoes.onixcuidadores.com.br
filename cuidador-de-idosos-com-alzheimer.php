<?php
    $title       = "Cuidador de idosos com Alzheimer";
    $description = "O serviço de cuidador de idosos com Alzheimer da Onix, levará ao necessitado, a ajuda e segurança que precisa, junta mento com um extremo profissionalismo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Sabemos que pessoas com Alzheimer necessitam de uma atenção maior em seus dia a dia e por isso, um serviço de cuidador de idosos com alzheimer é extremamente essencial aos mesmos. Até porque, quem possui compromissos diários, ou mais pessoas pra cuidar, não conseguem depositar 100% de atenção na maior parte do tempo. E é também para isso que serve o nosso cuidador de idosos com Alzheimer, para que o idoso tenha o profissionalismo, juntamente com a segurança e cuidado que precisam e merecem. Nós possuímos os melhores recursos para que nosso cuidador de idosos com Alzheimer consiga atender todas as necessidades postas a ele. E também porque os mesmos possuem grandes experiências não só no serviço de cuidador de idosos com Alzheimer, mas em todos os serviços que temos disponíveis em nossa empresa. Nós queremos sempre fazer com que nossos pacientes se sintam o mais confortáveis possível em nosso atendimento e para isso, fazemos o que for preciso para que isso ocorra. Como por exemplo, nós moldamos nossos horários conforme o cliente que venha nos consultar deseja e também disponibilizamos a troca de nossos funcionários, caso tenha uma falta de adaptação de nossos pacientes para com os mesmos. Durante o atendimento, nossos profissionais realizam atividades diárias, auxiliando os idosos no que for necessário, até mesmo por questão de saúde física. E em toda finalização de atendimento, é entregue um relatório à família e responsáveis, para que os mesmos possam ter ciência de como está sendo o processo de acompanhamento do cuidador. Para que você tenha o conforto que deseja e para que a pessoa que você cuida tenha a segurança que precisa, consulte os serviços da Onix.</p>

<h2>Mais detalhes sobre nosso cuidador de idosos com Alzheimer</h2>
<p>O nosso cuidador de idosos com Alzheimer, atende a domicílios, hospitais, clínicas e casas de repousos. Portanto, o que for mais viável para você, a nossa empresa te disponibiliza. Realize o seu orçamento em nosso site, de forma rápida e prática. Você e suas necessidades são nossas prioridades dentro da Onix.</p>


<h3>O melhor cuidador de idosos com Alzheimer</h3>
<p>Entre em contato com um cuidador de idosos com Alzheimer da Onix, através de nosso site mesmo, ou pelo nosso aplicativo exclusivo. Mas caso prefira, nós possuímos diversos meios de contatos para que você entre em contato conosco. Caso deseja nos fazer uma visita, estamos localizados em São Paulo e de portas abertas para te receber.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>