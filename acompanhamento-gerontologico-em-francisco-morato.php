<?php
$title       = "Acompanhamento Gerontológico em Francisco Morato";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O nosso Acompanhamento Gerontológico em Francisco Morato pode ser fornecido no ambiente em que os nossos pacientes necessitarem, seja em domicílios, casas de repousos, clínicas ou hospitais. Fale com um de nossos profissionais através de nossos site, para agendarmos a sua avaliação e fazermos o seu orçamento até mesmo sem compromisso. Será um prazer entregarmos nossos serviços de qualidade a você.</p>
<p>A empresa Onix Gestão Do Cuidado possui uma ampla experiência no segmento de Cuidado ao Idoso, de onde vem atuando em Acompanhamento Gerontológico em Francisco Morato com dedicação e proporcionando os melhores resultados, garantindo sempre a excelência. Assim, vem se destacando de forma positiva das demais empresas do mercado que vem atuando com total empenho em Acompanhante de Idosos, Cuidar de Idosos em Casas Particulares, Onde Encontrar Cuidadores de Idosos, Cuidador de Idosos 3 Vezes por Semana e Empresa de Acompanhante de Idosos. Entre em contato conosco.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>