<?php
    $title       = "Serviço de cuidadores de idosos";
    $description = "O serviço de cuidadores de idosos, da Onix está disponível a qualquer pessoa em qualquer ambiente. Portanto, não deixe de nos consultar. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>O serviço de cuidadores de idosos é extremamente importante para os familiares e responsáveis que não conseguem dedicar total atenção aos mesmos e também, para que eles tenham o devido cuidado que merecem e precisam. Em nosso serviço de cuidadores de idosos, estão presentes gerontólogos, enfermeiros e demais outros profissionais da saúde, que são extremamente capacitados para atenderem a qualquer tipo de necessidade. Durante o nosso serviço de cuidadores de idosos, são realizadas atividades diárias, onde os nossos profissionais auxiliam nossos pacientes no que for necessário, para que haja uma evolução. Nós apresentamos também relatórios diários para que a família e responsáveis tenham ciência de como os nossos trabalhos estão sendo atuados. Estamos sempre estudando novas técnicas e novos métodos, para que os mesmos sejam cada vez mais exclusivos em nossos serviços. Temos o objetivo de nos tornarmos cada vez mais referência com o serviço de cuidadores de idosos; e para isso, desde o seu primeiro contato conosco, nós estregamos nossos melhores atendimentos, para correspondermos todas as expectativas de nossos clientes para conosco. Queremos fazer com que nossos pacientes se sintam confortáveis em todo o processo de nosso acompanhamento e para isso fazemos o que for necessário. Como por exemplo, a nossa disponibilidade de troca de funcionário, caso haja uma falta de adaptação dos nossos pacientes para com o mesmo. E também, a disponibilidade de troca de horário, conforme for a rotina de nossos pacientes. Visamos sempre melhorarmos qualidade de vidas através dos nossos serviços, pois sabemos da importância que é viver uma vida saudável e com leveza.  </p>
<h2>Conheça mais sobre nosso serviço de cuidadores de idosos  </h2>
<p>Através de nosso serviço de cuidadores de idosos, os mesmos são acompanhados de forma profissional e qualificada, para que possamos atender todos os seus pedidos. Fazemos a questão de mantermos os nossos valores acessíveis, para que a qualquer momento em que precisarem de qualquer um de nossos serviços, os mesmos estejam ao alcance dos mesmos. Em nosso site, você pode realizar o seu orçamento de forma online e rápida, sem demais complicações.  </p>

<h3>O melhor serviço de cuidadores de idosos  </h3>
<p>Em nosso site, ou até mesmo através de nosso aplicativo, você pode agendar uma avaliação com um de nossos profissionais perante ao nosso serviço de cuidadores de idosos. Nós possuímos diversos meios de contatos, para que você possa falar conosco da forma em que desejar. Conte sempre com os serviços da Onix. Será um prazer te atender.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>