<?php
$title       = "Acompanhamento Hospitalar para Idoso em Capão Redondo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sabemos da importância que há no serviço de Acompanhamento Hospitalar para Idoso em Capão Redondo e através disso, deixamos os nossos valores acessíveis, para que a qualquer momento que você necessitar desse e demais serviços que disponibilizamos, nós sejamos a sua primeira opção. Tanto pela nossa qualidade, quanto pela acessibilidade de nossos valores. </p>
<p>Com sua credibilidade no mercado de Cuidado ao Idoso, proporcionando com qualidade, viabilidade e custo x benefício seja em Acompanhamento Hospitalar para Idoso em Capão Redondo quanto em Atenção Domiciliar ao Idoso, Acompanhamento Hospitalar, Alzheimer Cuidados de Enfermagem, Cuidador de Idosos para Dar Banho e Equipe Multiprofissional de Atenção Domiciliar, podendo dessa forma garantir seu sucesso no segmento em que atua de forma idônea e com altíssimo nível de qualidade a Onix Gestão Do Cuidado é a opção número um para você garantir o melhor no que busca!</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>