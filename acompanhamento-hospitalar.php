<?php
    $title       = "Acompanhamento hospitalar";
    $description = "O acompanhamento hospitalar da Onix é extremamente único e diferente de tudo o que você já viu. Entre em contato conosco para saber mais detalhes. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O serviço de acompanhamento hospitalar da ônix é essencial para os idosos que não conseguem exercer atividades com independência e que precisam de um acompanhamento integral ou de suporte, 24h. Nós fazemos o acompanhamento hospitalar em diversas instituições, Como hospitais casa de repouso entre outros. Nossos profissionais realizam um atendimento individual dando a devida atenção necessária independente da hora que for. Os mesmos possuem longos anos de experiência com acompanhamento hospitalar e também nos demais serviços que temos disponibilizados em nosso site. Todavia os mesmos atribuem os seus conhecimentos absorvidos, colocando-os em prática para que estejam aptos a qualquer tipo de pedido e necessidade que chegar até eles. Um dos nossos maiores princípios, é garantir que os idosos tenham suas necessidades básicas em qualquer instituição; o que nada mais é uma parte do nosso trabalho de acompanhamento hospitalar. Para que os familiares e os responsáveis tenham ciência de como os nossos serviços funcionam, nós realizamos relatórios diários para apresentarmos tudo o que foi realizado no dia e até mesmo para fazermos o acompanhamento de uma possível evolução. Nossos profissionais frequentemente realizam estudos para que as técnicas utilizadas em nossos serviços sejam cada vez mais exclusivas. E também, para que sejamos cada vez mais referência nesse mercado, para que todos possam ter a melhor experiência como cliente e paciente. Garantimos que desde seu primeiro contato conosco, você perceberá que a onix é altamente capacitada para atender a qualquer tipo de solicitação. Portanto, não perca essa oportunidade de melhor a qualidade de vida, com a melhor empresa que realiza a gestão de cuidados.</p>



<h2>Mais detalhes sobre nosso acompanhamento hospitalar</h2>

<p>Nós temos um aplicativo próprio da onix, onde você pode nos solicitar um acompanhamento hospitalar. Nós somos maleáveis a valores e também as suas necessidades. Como por exemplo caso seja necessário nós realizamos a troca de funcionários por falta de adaptação caso seja necessário e diversos outros fatores que moldamos para que o seu atendimento seja o melhor possível.</p>

<h3>A melhor opção de acompanhamento hospitalar</h3>
<p>Em nosso acompanhamento hospitalar nós os auxiliamos em atividades diárias para que os mesmos se tornem cada vez mais independentes em seus afazeres do dia a dia. Através do nosso site você conseguirá ter contato com de nossos especialistas para fazer a sua avaliação. Mas, caso prefira nós possuímos diversos meios de contatos onde você pode entrar em contato conosco e tirar qualquer dúvida que possuir perante os nossos serviços.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>