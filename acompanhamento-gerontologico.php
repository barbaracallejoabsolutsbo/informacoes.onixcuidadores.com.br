<?php
    $title       = "Acompanhamento gerontológico";
    $description = "O acompanhamento gerontológico da onix, levará conforto não só para quem está o recebendo, mas para os que puderem presenciar a evolução dos idosos após o mesmo";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p><br />Todos os nossos profissionais que realizam o acompanhamento gerontológico, possuem ensino superior e por isso, têm um vasto conhecimento no processo do envelhecimento e tudo o que engloba nesse fator. Em nosso acompanhamento gerontológico, nossos profissionais estimularão os idosos a exercerem tais atividades, para acompanhar fatores sociais, psicológicos e físicos; com o objetivo de fazer com que os mesmos tenham cada vez mais independência em suas atividades diárias, sozinhos ou em coletivo. Nossa meta é promovermos cada vez mais conforto e bem-estar para nossos pacientes e o nosso acompanhamento gerontológico é um ótimo caminho para que possamos alcançar a mesma. Esse acompanhamento, fará com que nossos profissionais conhecem cada vez mais nossos pacientes, para que então, encontrem respostas para ajudar na melhoria do cuidado para com os mesmos. Levando sempre um extremo conforto e segurança; não só para os idosos, mas para familiares e responsáveis. O acompanhamento gerontológico da ônix, é um atendimento único e personalizado, pois cada paciente possui suas necessidades de melhoria. Portanto, são aplicadas estratégias individuais, para que possamos recuperar a independência, qualidade de vida e a saúde dos mesmos. A cada dia, nós aprimoramos nossos serviços com o conhecimento que se é absorvido nos mesmos. Até mesmo para que nos tornemos referência com a qualidade de nossos cuidados de idosos. Desde seu primeiro contato com um de nossos profissionais, os mesmos te fornecem o melhor atendimento, tanto para conquistarmos sua confiança, tanto para que sempre que você estiver em busca de serviços de acompanhamento de idosos, você já tenha ciência que somos o único lugar que possui todos os recursos necessários, para que as suas necessidades sejam atendidas e correspondidas.</p>

<h2>Mais informações sobre nosso acompanhamento gerontológico</h2>
<p>Em nosso site, você poderá consultar os diversos serviços que temos disponíveis além do nosso acompanhamento gerontológico. Queremos sempre fazer com que possamos atender a todos os pedidos de nossos pacientes e para isso, nós fazemos o que for melhor a você. Como por exemplo, possibilidade de alterar horário de atendimento, também a possibilidade de troca de funcionário por falta de adaptação dos nossos pacientes com os mesmos... entre outros fatores.</p>

<h3>O melhor acompanhamento gerontológico para você</h3>
<p>Nós damos suporte em nosso acompanhamento gerontológico 24h, conforme for necessário. Você pode agendar uma avaliação conosco em nosso site, ou falar com diretamente com um especialista no mesmo. Nós possuímos diversas redes sociais para que você possa acompanhar nossos serviços de uma forma mais detalhada.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>