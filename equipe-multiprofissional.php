<?php
    $title       = "Equipe multiprofissional";
    $description = "A Onix possui uma equipe multiprofissional, com profissionais extremamente qualificados e experientes em suas áreas. Não deixe de conhecer mais sobre.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>A Onix conta com equipe multiprofissional, pois um dos nossos maiores objetivos, é mudarmos vidas para melhor através de nossos serviços. E para isso, nós fazemos o que for necessário, para que possamos alcançá-los. Desde o nosso princípio, nós buscamos evoluir cada vez mais como empresa. E sabemos da importância que é possuir uma equipe multiprofissional nos serviços de cuidadores de idosos. Contudo, nós contamos com os melhores profissionais, que possuem grandes experiências nesse ramo, independente de suas especialidades, para que possam atuar em nossa empresa. Em cada acompanhamento, nossa equipe multiprofissional elabora atividades, para que os nossos pacientes possam estimular seus comportamentos e se tornarem cada vez mais independentes em seus afazeres. Os profissionais da equipe multiprofissional da Onix, são instruídos a apresentarem relatórios diários à família e responsáveis, para que os mesmos possam tirar qualquer dúvida que possuírem perante aos nossos serviços. Priorizamos sempre o conforto e a segurança de nossos pacientes; e para isso, moldamos o que for necessário, para que isso ocorra. Seja a alteração de horários, ou até mesmo a possibilidade de troca dos nossos funcionários, por falta de adaptação de nossos pacientes, para com os mesmos. Vale lembrar que nós realizamos nossos atendimentos em diversos ambientes, como hospitais, casa de repouso, clínicas e até mesmo domicílios. Contudo, entre em contato com um de nossos profissionais o quanto antes, para nos apresentar todas as suas necessidades, para também, prestarmos nossos melhores serviços a você, mostrando também a melhor opção dos mesmos, que vão de acordo com a sua realidade. Estamos prontamente preparados para auxiliarmos nossos pacientes em qualquer situação. Será um prazer lhe comprovarmos toda a nossa teoria, em prática.  </p>

<h2>Conheça mais sobre nossa equipe multiprofissional </h2>
<p>Em nossa equipe multiprofissional, nós contamos com diversos profissionais. Sendo eles enfermeiros, gerontólogos e demais outros. E os mesmos possuem grandes conhecimentos nesse ramo, pois possuem longos anos de experiências, seguidos de grandes conhecimentos, para que possam exercer suas funções em nossa empresa com grande êxito e excelência. Garantimos que os nossos serviços são qualificados para qualquer necessidade que você vier recorrer até nós.  </p>

<h3>O melhor lugar para adquiri uma equipe multiprofissional  </h3>
<p>Uma equipe multiprofissional é extremamente importante para diversos quesitos, durante o acompanhamento de cuidados para idosos. E por termos essa noção, nós deixamos os valores de nossos serviços extremamente acessíveis, para que a qualquer momento que você precisar de qualquer serviço de cuidado para idosos, consulte os serviços da Onix.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>