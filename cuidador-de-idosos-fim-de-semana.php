<?php
    $title       = "Cuidador de idosos fim de semana";
    $description = "Cada cuidador de idosos fim de semana da Onix, possuem experiências nesse e demais serviços, portanto são altamente capacitados para recorrer a qualquer necessidade.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Onix, é a melhor empresa para você que está buscando por um cuidador de idosos fim de semana, onde as suas necessidades de encontrar um profissional qualificado e que prestará o atendimento necessário ao idoso, serão correspondidas. Desde o princípio de nossa empresa, o cuidador de idosos fim de semana da Onix, desenvolvem novas técnicas constantemente, para que estejamos cada vez mais aptos a qualquer paciente que vier recorrer a nós. E nós possuímos os melhores e mais atualizados recursos, para que sejamos cada vez mais referência no serviço de cuidador de idosos fim de semana e também nos demais outros que você poderá conhecer mais detalhadamente em nosso site. A Onix atua nesse ramo há longos anos e todos os profissionais contribuintes de nossa empresa possuem grandes experiências como cuidador de idosos fim de semana. Portanto, garantimos que você poderá apresentar todas as suas necessidades aos mesmos, pois eles são extremamente capacitados e qualificados para corresponderem a qualquer tipo de solicitação. Vale lembrar que nós prestamos nossos serviços em hospitais, domicílios, casas de repouso e clínicas. Portanto, independente do local desejado, qualquer um de nossos serviços será viável a você e ao seu alcance. Desde seu primeiro contato conosco, a necessidade do idoso se torna nossa prioridade e para que o mesmo se sinta confortável em todas as questões de nosso acompanhamento e atendimento, nós fazemos o que for preciso para que tal feito ocorra; moldamos nossos horários e até mesmo se for necessário, fazemos a troca de nossos funcionários para que haja uma melhor adaptação. Para que os familiares e responsáveis possam ter detalhes de como o nosso trabalho está sendo aplicado, os nossos especialistas realizam relatórios em cada dia, para que os mesmos saibam tudo o que foi feito.</p>

<h2>Mais detalhes sobre o cuidador de idosos fim de semana da Onix</h2>
<p>Para você que aos fins de semana têm compromissos e não pode prestar 100% de atenção ao idoso, o cuidador de idosos fim de semana da Onix, atenderá todos os seus pedidos com extremo profissionalismo e também, para que o idoso tenha o cuidado que merece.</p>

<h3>O melhor lugar para obter um cuidador de idosos fim de semana</h3>
<p>Desde o seu primeiro contato conosco, a sua confiança já será depositada em nós de primeira em nosso serviço de cuidador de idosos fim de semana. Pois um dos nossos maiores objetivos é mudarmos vida para melhor, através de nossos trabalhos que só trazem benefícios.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>