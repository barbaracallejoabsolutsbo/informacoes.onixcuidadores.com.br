<?php
    $title       = "Empresa terceirizada de cuidadores de idosos";
    $description = "A Onix, é uma empresa terceirizada de cuidadores de idosos. Possuímos diversos serviços, para diversos tipos de ambientes. Não deixe de nos consultar.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Adquirir uma empresa terceirizada de cuidadores de idosos com extrema qualidade e profissionalismo, é extremamente importante na hora da entrega dos serviços. E na Onix, uma empresa terceirizada de cuidadores de idosos, nós buscamos sempre inovar em nossas técnicas e métodos utilizados em nossos serviços, para que sejamos cada vez mais exclusivos e aprimor sempre nossas qualidades, para que nos tornemos cada vez mais referência para aqueles que já conhecem nossos serviços como empresa terceirizada de cuidadores de idosos; e para os que não conhecem também. Desde o nosso princípio, nós nos adaptamos a qualquer situação, para que nossos pacientes tenham o máximo de conforto e segurança ao presenciar nossos atendimentos. Moldamos nossos horários de acordo com a rotina de nossos pacientes, fazendo também um acompanhamento integral e até mesmo suporte 24h. Caso haja uma falta de adaptação de nossos pacientes para com os nossos profissionais, nós disponibilizamos também a troca dos mesmos, para que o acompanhamento aconteça de forma agradável para todos. Um dos maiores objetivos da Onix, empresa terceirizada de cuidadores de idosos, é melhorar qualidade de vidas, através de cada processo de atendimento que fornecemos. Nós contamos com profissionais gerontólogos e enfermeiros com grandes experiências nesse ramo e altamente capacitados para que estejam aptos a exercerem sus funções dentro de nossa empresa. Somos uma empresa com serviços completos e únicos, para que todos os que necessitam de cuidados para idosos a qualquer momento, possam contar com a Onix. Estamos aqui para auxiliarmos os idosos no que for necessário, para que os mesmos possam se tornar cada vez mais independentes em seus afazeres diários. Nós realizamos relatórios diários, conforme for o acompanhamento, para que a família e responsáveis possam ter ciência de como está ocorrendo cada processo.</p>
<h2>Mais informações sobre nossa empresa terceirizada de cuidadores de idosos</h2>
<p>Sabemos da importância que é adquiri os serviços de uma empresa terceirizada de cuidadores de idosos, de qualidade. Portanto, não temos problema em tirar toda e qualquer dúvida que você possuir perante aos nossos serviços. Fazemos a questão de mostrarmos em prática, toda a nossa teoria. Contudo, não perca mais tempo e adquira o quanto antes nossos serviços.</p>

<h3>A melhor opção de empresa terceirizada de cuidadores de idosos</h3>
<p>Através de nosso site, você pode falar diretamente com um dos profissionais de nossa empresa terceirizada de cuidadores de idosos. Portanto, não hesite ao nos consultar para tirarmos suas dúvidas e até mesmo realizarmos seu orçamento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>