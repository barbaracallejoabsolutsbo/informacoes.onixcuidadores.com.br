<?php
$title       = "Serviço de Cuidadores de Idosos no Jardim Iguatemi";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Realize o seu orçamento, até mesmo sem compromisso, o quanto antes perante ao nosso Serviço de Cuidadores de Idosos no Jardim Iguatemi, para que você veja em prática toda a nossa teoria. Priorizamos que os nossos valores sejam acessíveis, para que a qualquer momento que qualquer pessoa, precisar de nossos trabalhos, a mesma tenha acesso. Contudo, não adie ainda mais essa oportunidade única.</p>
<p>Nós da Onix Gestão Do Cuidado trabalhamos dia a dia para garantir o melhor em Serviço de Cuidadores de Idosos no Jardim Iguatemi e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Cuidado ao Idoso com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Cuidador de Idosos em Casa, Home Care Cuidador de Idosos, Acompanhamento Hospitalar, Empresa de Cuidadores de Idosos e Empresa de Cuidador de Idosos e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>