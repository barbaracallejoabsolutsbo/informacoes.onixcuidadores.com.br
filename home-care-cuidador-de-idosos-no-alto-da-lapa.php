<?php
$title       = "Home Care Cuidador de Idosos no Alto da Lapa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Na Onix, uma empresa de Home Care Cuidador de Idosos no Alto da Lapa, a cada ano de nossa experiência, nós atribuímos mas conhecimentos para que melhoremos a nossa qualidade a cada dia a mais. Nosso maior objetivo é fazer com que todos os nossos pacientes tenham o máximo de conforto ao consultar qualquer um de nossos serviços. E para isso, moldamos o que for necessário para alcançarmos o mesmo.</p>
<p>Com uma alta credibilidade no mercado de Cuidado ao Idoso, proporcionando com qualidade, viabilidade e custo x benefício em Home Care Cuidador de Idosos no Alto da Lapa, a empresa Onix Gestão Do Cuidado vem crescendo e mostrando seu potencial através, também de Equipe Multiprofissional de Atenção Domiciliar, Acompanhante de Idosos, Gerontologia Cuidado ao Idoso, Cuidador de Idosos com Fratura de Fêmur e Cuidador de Idosos com Alzheimer, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>