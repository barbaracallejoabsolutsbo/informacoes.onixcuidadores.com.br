<?php
    $title       = "Treinamentos para cuidadores em SP";
    $description = "Nossos treinamento para cuidadores em SP, te qualificarão ainda mais para que você tenha mais conhecimento com cuidados para idosos, Conte com a Onix.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A busca por cuidadores de idosos vêm crescendo cada vez mais. E para que você seja um destaque, com um diferencial, é essencial fazer parte de tratamentos para cuidadores em SP. E na Onix, você encontra treinamentos para cuidadores em SP, com recursos extremamente aptos e qualificados para que nossos clientes absorvam o máximo de conhecimento que puderem, para que os mesmos estavam prontos para qualquer situação que possa vir a ocorrer em seus trabalhos. Estamos sempre estudando o mercado de treinamentos para cuidadores em SP, para que sejamos cada vez mais únicos e exclusivos em nossas técnicas, até mesmo para nos tornarmos referência nesse ramo, para que mais pessoas possam ter acesso ao nosso serviço de qualidade. Nossos profissionais são extremamente qualificados, pois são diplomatas, para exercerem suas funções em nossa empresa. Sejam eles gerontólogos, enfermeiros e até mesmo os professores de nossos treinamentos para cuidadores em SP. Em nosso treinamento, nós mostramos aos nossos alunos que o objetivo do serviço de cuidador de idosos, é melhorar a qualidade de vida dos mesmos, para que tenham evoluções diárias e se tornarem cada vez mais independentes em seus afazeres diários. Durante o treinamento você atribuirá grandes técnicas passadas pelos nossos profissionais, para que junto com a Onix, você faça a diferença para mais vidas. Priorizamos sempre utilizarmos dos melhores produtos em nossos serviços e ensinamentos, para que qualquer pessoa que tenha o mínimo de contato conosco, veja o diferencial em nossa qualidade. Até mesmo para que sempre que precisarem de treinamentos para cuidadores em SP, atendimento e o que for, para idosos, saibam que a Onix é o único lugar onde suas necessidades podem ser correspondidas.</p>

<h2>Mais detalhes sobre nossos treinamentos para cuidadores em SP</h2>
<p>Sabemos a importância e essencialidade que há no serviço de cuidador. E ter profissionais qualificados para tal, é extremamente importante para que os familiares e responsáveis dos pacientes depositem suas confianças nos mesmos. E é por esse e demais motivos que os treinamentos para cuidadores em SP, é extremamente importante na vida daqueles que desejam seguir carreira nesse ramo.</p>

<h3>A melhor opção de treinamentos para cuidadores em SP</h3>
<p>Nós mantemos os valores de nossos treinamentos para cuidadores em SP, extremamente acessíveis, para que mais pessoas possam obter um conhecimento e profissionalismo de qualidade, através de nossos profissionais. Em nosso site, você pode realizar o seu orçamento de forma online e rápida; e até mesmo entrar em contato com um de nossos profissionais.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>