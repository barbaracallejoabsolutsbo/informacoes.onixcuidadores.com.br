<?php
$title       = "Cuidador de Idosos com Alzheimer em Jardim Presidente Dutra - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Cuidador de Idosos com Alzheimer em Jardim Presidente Dutra - Guarulhos é extremamente importante, pois os idosos que possuem essa doença, necessitam de mais atenção durante o dia. E com profissionais qualificados e experientes, você não terá demais preocupações. E na Onix, desde seu primeiro contato, automaticamente você depositará toda a sua confiança em nossos trabalhos pois verá que temos todos os recursos necessários.</p>
<p>Se você está em busca por Cuidador de Idosos com Alzheimer em Jardim Presidente Dutra - Guarulhos conheça a empresa Onix Gestão Do Cuidado, pois somos especializados em Cuidado ao Idoso e trabalhamos com variadas opções de produtos e/ou serviços para oferecer, como Cuidador de Idosos 3 Vezes por Semana, Onde Encontrar Cuidadores de Idosos, Cuidador de Idosos em Casa, Empresa de Acompanhante de Idosos e Agência de Home Care Cuidador. Sabendo disso, entre em contato conosco e faça uma cotação, temos competentes profissionais para dar o melhor atendimento possível para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>