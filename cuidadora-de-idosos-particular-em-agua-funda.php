<?php
$title       = "Cuidadora de Idosos Particular em Água Funda";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Cuidadora de Idosos Particular em Água Funda da Onix, atende em qualquer ambiente que você nos solicitar, sejam eles casas de repouso, hospitais, domicílios ou clínicas. Nós contamos com profissionais gerontólogos e enfermeiros extremamente qualificados, para que possam exercer suas funções com grande êxito, garantindo a satisfação de nossos pacientes. Não deixe de nos consultar para saber mais.</p>
<p>Com uma alta credibilidade no mercado de Cuidado ao Idoso, proporcionando com qualidade, viabilidade e custo x benefício em Cuidadora de Idosos Particular em Água Funda, a empresa Onix Gestão Do Cuidado vem crescendo e mostrando seu potencial através, também de Serviço de Acompanhamento de Idosos, Cuidador de Idosos Hospitalar, Cuidar de Idosos Fim de Semana, Empresa de Acompanhante de Idosos e Cuidador Hospitalar Preço, garantindo assim seu sucesso no mercado em que atua. Venha você também e faça um orçamento com um de nossos especialistas no ramo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>