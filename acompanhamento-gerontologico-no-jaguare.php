<?php
$title       = "Acompanhamento Gerontológico no Jaguaré";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Onix é uma empresa especializada em serviços de Acompanhamento Gerontológico no Jaguaré, pois contamos com profissionais com grandes experiências; longos anos de estudos e diplomatas, para que então, os mesmos estejam aptos para corresponderem a qualquer tipo de necessidade que chegar até nós. Lembrando que realizamos atividades e relatórios diários para fazer a análise da evolução de nossos pacientes. </p>
<p>Como uma empresa de confiança no mercado de Cuidado ao Idoso, unindo qualidade, viabilidade e valores acessíveis e vantajosos para quem procura por Acompanhamento Gerontológico no Jaguaré. A Onix Gestão Do Cuidado vem crescendo e mostrando seu potencial através de Cuidador de Idosos com Fratura de Fêmur, Cuidador de Idosos em Casa, Cuidar de Idosos em Casas Particulares, Cuidar de Idosos Fim de Semana e Empresa Terceirizada de Cuidadores de Idosos, garantindo assim seu sucesso no mercado em que atua sempre com excelência e confiabilidade que mostra até hoje.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>