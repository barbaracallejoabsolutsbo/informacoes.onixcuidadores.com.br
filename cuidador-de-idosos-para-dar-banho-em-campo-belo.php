<?php
$title       = "Cuidador de Idosos para Dar Banho em Campo Belo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Além do serviço de Cuidador de Idosos para Dar Banho em Campo Belo, nós fornecemos serviços similares aos nossos clientes, para que possamos auxilia-los no que for necessário. Nós fornecemos nossos serviços em ambientes como, domicílio, hospitais, casa de repouso e clínicas. Portanto, nos apresente a opção que for mais viável a você, para que possamos te entregar nossos melhores trabalhos.</p>
<p>Com a Onix Gestão Do Cuidado proporcionando o que se tem de melhor e mais moderno no segmento de Cuidado ao Idoso consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Acompanhamento Hospitalar para Idoso, Cuidador de Idosos para Dar Banho, Serviços de Transporte para Idosos, Acompanhante de Idosos e Cuidador de Idosos em Casa nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Cuidador de Idosos para Dar Banho em Campo Belo.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>