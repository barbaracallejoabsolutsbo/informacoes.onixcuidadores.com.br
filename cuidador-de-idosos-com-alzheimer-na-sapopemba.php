<?php
$title       = "Cuidador de Idosos com Alzheimer na Sapopemba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Cuidador de Idosos com Alzheimer na Sapopemba é extremamente importante, pois os idosos que possuem essa doença, necessitam de mais atenção durante o dia. E com profissionais qualificados e experientes, você não terá demais preocupações. E na Onix, desde seu primeiro contato, automaticamente você depositará toda a sua confiança em nossos trabalhos pois verá que temos todos os recursos necessários.</p>
<p>Conseguindo ofertar Serviço de Acompanhamento de Idosos, Acompanhamento Gerontológico, Cuidador de Idosos para Dar Banho, Empresa de Acompanhante de Idosos e Acompanhante de Idosos a Onix Gestão Do Cuidado proporciona Cuidador de Idosos com Alzheimer na Sapopemba ideal para atender às reais necessidades de seus clientes e parceiros, assim comprovando o fato de ser a empresa mais completa e eficiente do mercado de Cuidado ao Idoso. Unindo a experiência que nossa empresa traz com profissionais competentes conseguimos proporcionar o melhor para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>