<?php
$title       = "Cuidador de Idosos com Alzheimer no Jardim Paulistano";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O nosso Cuidador de Idosos com Alzheimer no Jardim Paulistano presta serviço nos mais variados ambientes para que possamos atender a mais pessoas, cada vez mais. Consulte um de nossos profissionais para nos apresentar suas necessidades e então levarmos nossos trabalhos em seu domicílio, casa de repouso, hospitais ou clínica. Nossa meta é melhorarmos a qualidade de vida de nossos pacientes, independente da onde for.</p>
<p>Você procura por Cuidador de Idosos com Alzheimer no Jardim Paulistano? Contar com empresas especializadas no segmento de Cuidado ao Idoso é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Onix Gestão Do Cuidado é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Cuidadora de Idosos Particular, Quanto Custa Cuidador 24 horas, Empresa Terceirizada de Cuidadores de Idosos, Acompanhamento Hospitalar e Atenção Domiciliar ao Idoso e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>