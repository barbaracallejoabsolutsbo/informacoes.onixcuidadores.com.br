<?php
$title       = "Acompanhamento Hospitalar para Idoso na Vila Buarque";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Sabemos da importância que há no serviço de Acompanhamento Hospitalar para Idoso na Vila Buarque e através disso, deixamos os nossos valores acessíveis, para que a qualquer momento que você necessitar desse e demais serviços que disponibilizamos, nós sejamos a sua primeira opção. Tanto pela nossa qualidade, quanto pela acessibilidade de nossos valores. </p>
<p>Como líder do segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado se dispõe a oferecer uma excelente assessoria nas questões de Serviço de Cuidadores de Idosos, Cuidador de Idosos com Fratura de Fêmur, Serviço de Acompanhamento de Idosos, Empresa Terceirizada de Cuidadores de Idosos e Home Care Idosos Preço sempre com muita qualidade e dedicação aos seus clientes e parceiros. Por contarmos com uma equipe profissionalmente competente para proporcionar o melhor em Acompanhamento Hospitalar para Idoso na Vila Buarque ganhamos a confiança e preferência de nossos clientes e parceiros.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>