<?php
    $title       = "Cuidador de idosos com fratura de fêmur";
    $description = "Garanta já o serviço de cuidador de idosos com fratura de fêmur da Onix, para que vidas possam ser mudadas para melhor, através de nossos trabalhos. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    

<p>Alguns idosos precisam de mais atenção do que outros, ainda mais quando se há uma lesão. E é nesses casos, que o serviço de cuidador de idosos com fratura de fêmur da Ônix, é essencial tanto para o idoso, quanto à família e responsáveis que cuidam do mesmo. Nosso cuidador de idosos com fratura de fêmur, tanto gerontologos quanto enfermeiros, exercem atividades diárias conforme as necessidades de nossos pacientes, para que os mesmos possam ter um quadro de evolução. E também, o cuidador de idosos com fratura de fêmur, após cada dia de avaliação e cuidado, realizam relatórios diários para que a família e responsáveis possam acompanhar o quando de evolução do paciente. Contudo, não se preocupe, pois através de nosso serviço em prática, você automaticamente depositará toda a sua confiança, pois como já citado, lhe provaremos isso. Garantimos a nossa qualidade nas técnicas utilizadas de nosso cuidador de idosos com fratura de fêmur e também no atendimento dos mesmos. Em nossa empresa, há profissionais extremamente capacitados para exercerem suas funções, pois nossos gerontólogos e enfermeiros são diplomatas e além de tudo, possuem grandes experiências nesse ramo. Contudo, não perca essa oportunidade de fazer com que quem precise, tenha acesso aos nossos serviços e até mesmo para você não ter preocupações. Nosso maior objetivo é melhorarmos a qualidade de vida de nossos pacientes através de nossos trabalhos e nós fazemos o possível para que isso ocorra. Um exemplo, é a nossa disponibilidade de troca de funcionários casa haja uma falta de adaptação do idoso com o mesmo e também nossos horários que são maleáveis de acordo com as necessidades de nossos pacientes.</p>

<h2>Mais informações sobre nosso cuidador de idosos com fratura de fêmur</h2>
<p>Para que todos possam ter acesso a um cuidador de idosos com fratura de fêmur, de qualidade, a Onix faz a questão de manter seus valores acessíveis, para que a qualquer momento que precisarem, os mesmos possam nos consultar.</p>

<h3>A melhor opção de cuidador de idosos com fratura de fêmur</h3>
<p>Nós temos o nosso próprio aplicativo para que você possa agendar uma avaliação com o nosso cuidador de idosos com fratura de fêmur; e em nosso site você pode realizar o mesmo. Mas caso prefira, nós possuímos diversos meios de contatos que estão disponibilizados em nosso site, para que você possa falar diretamente com um de nossos representantes. Não deixe de falar conosco para que possamos juntos, melhorarmos qualidades de vidas.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>