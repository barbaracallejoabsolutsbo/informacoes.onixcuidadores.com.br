<?php
$title       = "Cuidador de Idosos com Alzheimer em Embu das Artes";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Cuidador de Idosos com Alzheimer em Embu das Artes é extremamente importante, pois os idosos que possuem essa doença, necessitam de mais atenção durante o dia. E com profissionais qualificados e experientes, você não terá demais preocupações. E na Onix, desde seu primeiro contato, automaticamente você depositará toda a sua confiança em nossos trabalhos pois verá que temos todos os recursos necessários.</p>
<p>Com a Onix Gestão Do Cuidado proporcionando o que se tem de melhor e mais moderno no segmento de Cuidado ao Idoso consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Empresa Terceirizada de Cuidadores de Idosos, Home Care Cuidador de Idosos, Serviço de Acompanhamento de Idosos, Serviço de Cuidadores de Idosos e Cuidador de Idosos Fim de Semana nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Cuidador de Idosos com Alzheimer em Embu das Artes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>