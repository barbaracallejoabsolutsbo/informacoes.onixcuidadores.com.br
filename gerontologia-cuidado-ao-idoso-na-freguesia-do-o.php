<?php
$title       = "Gerontologia Cuidado ao Idoso na Freguesia do Ó";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nosso serviço de Gerontologia Cuidado ao Idoso na Freguesia do Ó, nossos profissionais realizam atividades diárias com os nossos pacientes, para que os mesmos tenham constante evoluções em seus quadros e possam realizar seus afazeres diários sem nenhum empecilho o quanto antes. Nos adaptamos a rotina de nossos pacientes, moldando sempre o que for necessário para garantir o bem-estar dos mesmos.</p>
<p>A Onix Gestão Do Cuidado é uma empresa que se destaca no setor de Cuidado ao Idoso, pois oferece não somente Diária de um Cuidador de Idoso, Cuidador Hospitalar Preço, Cuidar de Idosos Fim de Semana, Cuidador de Idosos Hospitalar e Atenção Domiciliar ao Idoso, mas também, Gerontologia Cuidado ao Idoso na Freguesia do Ó com o intuito de atender às mais exigentes solicitações. Entre em contato e faça uma cotação com um de nossos competentes profissionais e assim comprove que somos a empresa que trabalha com o foco na qualidade e satisfação de nossos clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>