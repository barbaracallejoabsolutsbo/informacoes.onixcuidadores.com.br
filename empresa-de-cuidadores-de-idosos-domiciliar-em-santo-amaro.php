<?php
$title       = "Empresa de Cuidadores de Idosos Domiciliar em Santo Amaro";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Consultar uma Empresa de Cuidadores de Idosos Domiciliar em Santo Amaro é extremamente necessário para você que busca levar uma melhoria na qualidade de vida do idoso. Estamos sempre atualizando os métodos utilizados em nosso atendimento, para que possamos não só atender as expectativas de nossos pacientes e sim, superá-las. Estamos disponíveis a qualquer momento.</p>
<p>Na busca por uma empresa qualificada em Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha com as melhores vantagens para o que você vem buscando. Além de fornecedor Cuidador de Idosos com Alzheimer, Empresa Terceirizada de Cuidadores de Idosos, Cuidador de Idosos em Casa, Serviço de Acompanhamento de Idosos e Cuidador de Idosos com Fratura de Fêmur com o melhor custo x benefício da região, nós temos como missão garantir agilidade, qualidade e dedicação no que vem realizando para garantir a satisfação de seus clientes que buscam por Empresa de Cuidadores de Idosos Domiciliar em Santo Amaro.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>