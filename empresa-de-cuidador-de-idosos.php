<?php
    $title       = "Empresa de cuidador de idosos";
    $description = "A Onix é a melhor empresa de cuidador de idosos da região. Portanto, não deixe de entrar em contato conosco para garantir um de nossos serviços. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Caso você esteja em busca por uma empresa de cuidador de idosos com o melhor atendimento do mercado e que sempre corresponde as necessidades expectativas dos pacientes, encontrou o lugar ideal.  Estamos sempre aprimorando os nossos serviços com novas técnicas exclusivas, para que nos tornemos cada vez mais referência como empresa de cuidador de idosos, fornecendo a qualidade de nossos serviços para cada vez mais pessoas. A cada trabalho, nossos profissionais adquirem mais experiências para que entreguemos um trabalho cada vez melhor. Como empresa de cuidador de idosos, ao garantia qualquer um de nossos serviços, o nosso paciente consequentemente terá uma melhoria de sua qualidade de vida, pois os nossos profissionais exercem atividades diárias para estimularem os mesmos a se tornarem cada vez mais independentes em seus afazeres diários. E ao final de cada dia, de nosso acompanhamento, nossos profissionais realizam relatórios para que possamos acompanhar detalhadamente a evolução de nossos pacientes. Nós somos uma empresa de cuidador de idosos com serviços completos, portanto não perca essa oportunidade única de garantir os nossos serviços e atendimento. Nós temos uma expansão em ambientes que nossos serviços podem ser levados, como por exemplo em hospitais, domicílios, casas de repouso e até mesmo clínicas. Fazemos o que for possível para que todos possam ter acesso ao nosso serviço de extrema qualidade excelência. Um dos nossos maiores princípios é fazer com que os nossos pacientes sintam o máximo de conforto ao usufruir nossos serviços; e para isso, nos moldamos no que for necessário para que isso ocorra.</p>

<h2>Conheça mais sobre a nossa empresa de cuidador de idosos</h2>
<p>A nossa empresa de cuidador de idosos possui um aplicativo próprio para facilitar o seu acesso a nós. Também, há diversos meios de contatos disponíveis em nosso site, para que você fale diretamente com um de nossos representantes. Em nosso site, você também pode agendar uma avaliação, para que os nossos profissionais vejam qual de nossos serviços é o mais apto para o paciente.</p>

<h3>A melhor opção de empresa de cuidador de idosos</h3>
<p>Sabemos da importância e urgência que há ao recorrer por uma empresa de cuidador de idosos, portanto, nós estamos sempre disponíveis para que você possa falar com um de nossos representastes, para que os mesmos te auxiliem no que for necessário. Nós aguardamos ansiosamente pelo seu contato, para que possamos mudar a vida de quem você ama e cuida. Não perca mais tempo e fale conosco o quanto antes.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>