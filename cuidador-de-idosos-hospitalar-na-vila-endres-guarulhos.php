<?php
$title       = "Cuidador de Idosos Hospitalar na Vila Endres - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nosso Cuidador de Idosos Hospitalar na Vila Endres - Guarulhos, acompanha nossos pacientes no que for necessário, seja com suporte 24h ou com um acompanhamento integral. Nossos profissionais gerontólogos e enfermeiros entregam aos familiares e responsáveis, relatórios ao fim de cada de de acompanhamento, para que todos possam ter ciência de como os trabalhos da Onix, são aplicados. </p>
<p>A empresa Onix Gestão Do Cuidado é destaque entre as principais empresas do ramo de Cuidado ao Idoso, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Cuidador de Idosos Hospitalar na Vila Endres - Guarulhos do mercado. Ainda, possui facilidade com Empresa de Cuidador de Idosos, Cuidador de Idosos Fim de Semana, Cuidar de Idosos a Noite, Acompanhante de Idosos e Treinamentos para Cuidadores de Idosos mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>