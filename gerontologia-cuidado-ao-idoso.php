<?php
    $title       = "Gerontologia cuidado ao idoso";
    $description = "A gerontologia cuidado ao idoso da Onix, é um serviço com métodos únicos, para que nossos pacientes se sintam cada vez mais confortáveis em nossos atendimentos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O serviço de gerontologia cuidado ao idoso, é extremamente importante e necessário, para que o idoso tenha todo o acompanhamento necessário, com um profissional especialista, consequentemente capacitado para atender a qualquer tipo de situação. A Onix, disponibiliza uma gerontologia cuidado ao idoso, conforme for necessário no atendimento, onde os profissionais realizam atividades diárias, auxiliando-os no que for necessário, para que os mesmos possam cada vez mais conquistar suas independências nos afazeres do dia a dia. Nossos profissionais são instruídos a apresentarem relatórios após cada acompanhamento, para que os familiares e responsáveis, possam ter ciência de como o nosso serviço de gerontologia cuidado ao idoso, está sendo aplicado. Em nossa empresa, nós contamos com gerontólogos, enfermeiros e demais profissionais da saúde, para que possamos recorrer a qualquer tipo de necessidade que chegar até nós. Através do nosso serviço e atendimento de gerontologia cuidado ao idoso, nós temos o objetivo de melhorarmos qualidade de vidas, ajudando no quer for necessário para que isso ocorra. Queremos fazer com que nossos pacientes sintam o máximo de conforto ao presenciar qualquer um de nossos atendimentos e para isso, nós modificamos o que precisar, para ficar de acordo com as vontades de nossos pacientes. Caso seja necessário, nós fazemos a alteração de nossos funcionários por falta de adaptação de nossos pacientes. E também, moldamos nossos horários, conforme suas rotinas. Vale lembrar que qualquer um de nossos serviços prestados, podem ser fornecidos em diversos ambiente. Como casas de repouso, hospitais, clínicas e até mesmo no próprio domicílio. Portanto, consulte nossos profissionais para apresentarmos a opção que é mais viável a você.</p>

<h2>Mais informações sobre nosso serviço de gerontologia cuidado ao idoso</h2>
<p>Sabemos da qualidade que há em nosso serviço de gerontologia cuidado ao idoso e através disso, queremos deixá-lo cada vez mais acessível para todos aqueles que nos consultarem. Para isso, nós mantemos nossos preços baixos, porém melhorando sempre a nossa qualidade e inovando em nossas técnicas. Até mesmo para que desde o seu primeiro contato conosco, você já saiba que temos todos os recursos necessários para atender a qualquer tipo de solicitação; com um preço acessível.</p>

<h3>A melhor gerontologia cuidado ao idoso</h3>
<p>Em nosso site, você pode entrar em contato com um de nossos representantes, até mesmo para agendar uma avaliação de gerontologia cuidado ao idoso. Estamos sempre disponíveis para que você possa tirar qualquer dúvida que possuir perante aos nossos serviços prestados. Não deixe de entrar em contato conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>