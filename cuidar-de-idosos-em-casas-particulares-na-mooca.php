<?php
$title       = "Cuidar de Idosos em Casas Particulares na Mooca";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>No serviço de Cuidar de Idosos em Casas Particulares na Mooca, os nossos profissionais realizam atividades diárias com os pacientes, para estimularem os mesmos cada vez mais em suas atividades do dia a dia, fazendo com que se tornem cada vez mais independentes também. Entregamos relatórios diários em cada acompanhamento, para que os responsáveis possam fazer suas análises perante aos nossos trabalhos.</p>
<p>Entre em contato com a Onix Gestão Do Cuidado se você busca por Cuidar de Idosos em Casas Particulares na Mooca. Somos uma empresa especializada com foco em Atenção Domiciliar ao Idoso, Treinamentos para Cuidadores de Idosos, Cuidador de Idosos Fim de Semana, Onde Encontrar Cuidadores de Idosos e Cuidador de Idosos em Casa onde garantimos o melhor para nossos clientes, uma vez que, contamos com o conhecimento adequado para o ramo de Cuidado ao Idoso. Entre em contato e faça um orçamento com um de nossos especialistas e garanta o melhor custo x benefício do mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>