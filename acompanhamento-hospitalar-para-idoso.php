<?php
    $title       = "Acompanhamento hospitalar para idoso";
    $description = "Conosco, você obterá o melhor acompanhamento hospitalar para idoso. Portanto, não perca essa oportunidade de garantir esse e demais serviços da Onix. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Não só o nosso serviço de acompanhamento hospitalar para idoso mas todos os serviços que temos disponíveis são executados pois treino profissionalismo e qualidade para que possamos responder todas as suas necessidades e tudo que nos é solicitado. Os nossos profissionais possuem grande conhecimento e acompanhamento hospitalar para idoso portanto os mesmos estão aptos para atenderem A qualquer tipo de solicitação. Até mesmo para que nos tornemos Cada vez mais referência com a exclusividade de nossa qualidade; tanto no atendimento quanto da forma em que fornecemos o nosso acompanhamento hospitalar para idoso. Desde o princípio da nossa empresa o nosso objetivo é fazer com que mais pessoas tenham acesso a todos os serviços que disponibilizamos em nosso site E para isso nós fazemos o que for necessário para que as necessidades de nossos pacientes posso ser correspondidos através de nossos trabalhos. Um exemplo disso é a possibilidade de alterar o horário de atendimento para nos adaptarmos o que for melhor para os pacientes, entre outros exemplos que você pode conferir em nosso site. Nós utilizamos dos melhores recursos e possuímos os melhores profissionais em nossa empresa, dentre gerantólogos e enfermeiros, Para que possamos atingir o objetivo de melhorar a qualidade de vida de nossos pacientes. Em nosso site você pode realizar seus orçamentos de forma on-line e falar diretamente com um de nossos especialistas. Entre em contato conosco até mesmo para agendarmos uma avaliação que será o primeiro passo para a melhoria de vida da pessoa que você ama e deseja cuidar. Afirmamos que desde o seu primeiro contato conosco, você verá que recorreu ao lugar correto.</p>

<h2>Mais informações sobre o nosso acompanhamento hospitalar para idoso</h2>
<p>Nosso objetivo é fazer com que cada vez mais, mais pessoas tenham acesso a qualidade de nosso acompanhamento hospitalar para idoso; e para isso, fazemos a questão de mantermos nossos valores acessíveis, para que a qualquer momento que necessitarem de nossos serviços, os mesmos estejam aptos, sem levar nenhum prejuízo financeiro. Conte sempre com os serviços de nossos profissionais, para que quem você ama e cuida, tenha uma vida cada vez mais saudável e ativa. Com o acompanhamento hospitalar para idoso correto.</p>


<h3>O melhor acompanhamento hospitalar para idoso</h3>
<p>Em nosso site há diversos meios de contatos, para que você possa falar com um de nossos representantes perante ao nosso acompanhamento hospitalar para idoso, mas nós estamos localizados em São Paulo. Portanto, se deseja nos fazer uma visita, aguardamos ansiosamente por você e pelo seu contato.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>