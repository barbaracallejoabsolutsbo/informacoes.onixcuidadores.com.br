<?php
$title       = "Home Care Cuidador de Idosos em Água Funda";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Obter o serviço de Home Care Cuidador de Idosos em Água Funda, é importante e necessário para que o idoso tenha todo o cuidado que precisam e também para que seus objetivos para conosco, sejam alcançados. Ao fim de cada atividade e acompanhamento, nossos profissionais entregam relatórios para que  os familiares e responsáveis possam ger ciência de como os nossos trabalhos estão sendo aplicados.
</p>
<p>Sendo referência no ramo Cuidado ao Idoso, garante o melhor em Home Care Cuidador de Idosos em Água Funda, a empresa Onix Gestão Do Cuidado trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Alzheimer Cuidados de Enfermagem, Cuidador Hospitalar Preço, Quanto Custa Cuidadores de Idosos, Cuidador de Idosos para Dar Banho e Acompanhante de Idosos para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>