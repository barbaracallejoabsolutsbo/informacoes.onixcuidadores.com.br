<?php
$title       = "Alzheimer Cuidados de Enfermagem no Alto da Lapa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Alzheimer Cuidados de Enfermagem no Alto da Lapa é importante para os familiares e responsáveis que acabam não podendo depositar suas atenções a todo momento ao idoso, por conta de seus afazeres diários. E nós estamos aqui para ajudarmos tanto na vida de nossos pacientes, quanto na vida do familiar e responsável. Portanto, não deixe de conhecer mais detalhadamente sobre esse nosso serviço.</p>
<p>A Onix Gestão Do Cuidado, além de ser especializados em Acompanhamento Hospitalar, Cuidar de Idosos a Noite, Acompanhamento Gerontológico, Cuidador de Idosos Hospitalar e Empresa de Cuidadores de Idosos, atuando no mercado de Cuidado ao Idoso com a missão de oferecer sempre o melhor para seus clientes, de forma que seus objetivos sejam alcançados. Além disso, contamos com profissionais competentes visando prestar o melhor atendimento possível em Alzheimer Cuidados de Enfermagem no Alto da Lapa para ser sempre a opção número um de nossos parceiros e clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>