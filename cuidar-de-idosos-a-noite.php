<?php
    $title       = "Cuidar de idosos a noite";
    $description = "O serviço de cuidar de idosos a noite da Onix, é capacitado para que você possa depositar toda a sua confiança no mesmo. Nos consulte o quanto antes!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O nosso serviço de cuidar de idosos a noite é essencial para aqueles idosos que possuem alguma vulnerabilidade a mais e necessitam de um acompanhamento de um profissional qualificado. E até mesmo, para que a família e responsáveis não tenham demais preocupações com os mesmos. Nós trabalhamos com profissionais gerontólogos e enfermeiros que possuem grandes experiências que é cuidar de idosos a noite. Também estão altamente capacitados e preparados para qualquer situação que for solicitado aos mesmos. Nosso serviço de cuidar de idosos a noite tem um suporte 24h00, portanto seja por uma forma emergencial ou planejada, você pode consultar o nossos serviço a qualquer momento. Os profissionais da Ônix prestam serviços de cuidar de idosos a noite em domicílios, hospitais, casas de repouso e até mesmo em clínicas. Portanto independente do ambiente que você precisa desse e demais serviços que nós disponibilizamos, você conseguirá obtê-lo. Estamos sempre estudando-os para utilizarmos das técnicas mais atualizados, para que possamos atender e até mesmo superar as expectativas de nossos pacientes, para com os nossos serviços. Nós sempre fazemos o que for necessário para que os nossos paciente se adaptem aos nossos funcionários, mas caso isso não ocorra nós disponibilizamos também a troca dos mesmos para que os nossos pacientes possam se sentir o mais confortáveis possíveis, durante o acompanhamento. Um dos nossos maiores objetivos, é melhoramos a qualidade de vida de mais pessoas através de qualquer um de nossos serviços. E para isso nós sempre trocamos nossos melhores trabalhos para que possamos nos tornar referência às pessoas e para que quando as mesmas pensarem em cuidados para idosos, pensem na Onix. Garantimos que ao nos consultar, você não precisará se preocupar com o nosso paciente, pois nós também fazemos a questão de que você tem um acompanhamento detalhado de nossos serviços, através de nossos relatórios.</p>

<h2>Mais detalhes sobre nosso serviço de cuidar de idosos a noite</h2>
<p>Será um prazer fazer com que a pessoa que você cuida e ama, possa receber nosso serviço de cuidar de idosos a noite, com a mais alta qualidade. Sabemos da importância que há nesse trabalho e para isso, mantemos os nossos valores acessíveis para que você possa nos consultar a qualquer momento que desejar/precisar</p>

<h3>A melhor opção de empresa para cuidar de idosos a noite</h3>
<p>Fale o quanto antes diretamente com um de nossos especialistas, para tirar qualquer dúvida que possuir sobre como o nosso serviço de cuidar de idosos a noite funciona.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>