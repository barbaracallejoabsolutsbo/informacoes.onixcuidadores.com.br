<?php
    $title       = "Cuidador de idosos hospitalar";
    $description = "O cuidador de idosos hospitalar da Onix, levará a você e ao idoso necessitado, todo o apoio e atenção necessária para acompanharmos o mesmo em tal situação. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Se você está em busca de um cuidador de idosos hospitalar, que corresponderá as necessidades que o idoso preciso, com profissionalismo, cuidado e a atenção , a Onix é o lugar totalmente capacitado para tal. Nós somos especialistas com cuidador de idosos hospitalar e demais serviços desse ramo, pois estamos há longos anos fornecendo nossos trabalhos para o máximo de pessoa possíveis. Garantimos que conosco, você sentirá total segurança quando presenciar o nosso serviço de cuidador de idosos hospitalar. Ao consultar nosso site, você verá que temos outros serviços disponíveis perante a cuidado de idosos. Portanto, independente do momento e para o que deseja, os nossos serviços são aptos a você e ao necessitado. Consulte o quanto antes um de nossos profissionais para consultar nosso cuidador de idosos hospitalar ou um de nossos serviços que for mais viável a você. Nesse serviço em específico, nossos profissionais, sejam gerontólogos ou enfermeiros, fazem todo o acompanhamento necessário, auxiliando-os em atividades diárias para que possamos ajudar em suas evoluções. São realizados também relatórios diários para que os responsáveis e familiares do paciente tenham ciência de como o nosso trabalho está sendo aplicado na vida dos mesmos. Realizamos diversos estudos diariamente, para que estejamos cada vez mais preparados para qualquer pedido de nossos pacientes para conosco e até mesmo para que a qualidade de nossos serviços seja única nesse mercado. Nossa meta é nos tornarmos referência em cuidados para idosos, para que mais pessoa possam conhecer e ter acesso ao trabalho que os mesmos merecem. Sabemos a importância que é fazer com que nossos pacientes se sintam confortáveis durante o atendimento de nossos cuidadores e para isso, nos moldamos no que for necessário para que isso ocorra.</p>

<h2>Conheça mais sobre nosso cuidador de idosos hospitalar</h2>
<p>Cada cuidador de idosos hospitalar da Onix possui grandes experiências nesse serviço e em demais outros desse ramo, portanto, são altamente qualificados para atenderem a qualquer tipo de solicitação. Desde seu primeiro contato conosco, nós lhe entregaremos nosso melhor atendimento, para que sempre que você precisar de um dos nossos serviços, você mesmo já saiba que somos o lugar ideal onde você pode depositar suas necessidades.</p>
<h3><br />A melhor opção de um cuidador de idosos hospitalar</h3>
<p>Nós possuímos diversos meios de contatos, como até mesmo nosso aplicativo exclusivo, para que você possa falar diretamente com um de nossos profissionais para tirar qualquer dúvida perante ao nosso serviço de cuidador de idosos hospitalar.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>