<?php
$title       = "Agência de Home Care Cuidador em Rio Pequeno";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A nossa Agência de Home Care Cuidador em Rio Pequeno, faz o que for necessário, para que nossos pacientes se sintam confortável durante qualquer tipo de acompanhamento. Todavia, moldamos o que for necessário em nossa agenda ou no quadro de nossos funcionários, para que possamos levar um bem-estar aos mesmos. Desde seu primeiro contato conosco, as suas necessidades tornam-se nossas prioridades.</p>
<p>Especialista no segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Agência de Home Care Cuidador em Rio Pequeno. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Cuidar de Idosos em Casas Particulares, Cuidador de Idosos Fim de Semana, Gerontologia Cuidado ao Idoso, Acompanhamento Gerontológico e Cuidador de Idosos 3 Vezes por Semana e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>