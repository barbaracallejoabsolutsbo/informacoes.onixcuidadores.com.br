<?php
$title       = "Acompanhante de Idosos em Santos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Acompanhante de Idosos em Santos é extremamente necessário para você que deseja melhorar a qualidade de vida da pessoa que você ama. Durante a prestação dos nossos serviços, serão realizadas atividades diárias com o idoso, juntamente com relatórios diários que serão apresentados a família e responsáveis, para que juntos, possam fazer o acompanhamento de possíveis evoluções dos pacientes.</p>
<p>Na busca por uma empresa qualificada em Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha com as melhores vantagens para o que você vem buscando. Além de fornecedor Agência de Home Care Cuidador, Cuidador de Idosos com Fratura de Fêmur, Empresa de Acompanhante de Idosos, Cuidar de Idosos a Noite e Empresa Terceirizada de Cuidadores de Idosos com o melhor custo x benefício da região, nós temos como missão garantir agilidade, qualidade e dedicação no que vem realizando para garantir a satisfação de seus clientes que buscam por Acompanhante de Idosos em Santos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>