<?php
    $title       = "Empresa de cuidadores de idosos em São Paulo";
    $description = "Somos uma empresa de cuidadores de idosos em São Paulo, com serviços completos e que corresponderão a qualquer necessidade. Conte conosco.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Onix, busca cada vez mais se inovar como empresa de cuidadores de idosos em São Paulo. E para isso, estamos sempre buscando e estudando novas técnicas, para que possamos nos tornar cada vez mais exclusivos nos serviços prestados nesse ramo. Até mesmo para que sejamos uma empresa de cuidadores de idosos em São Paulo, referência para outras pessoas, para que quando precisarem de qualquer serviço para idosos, a nossa empresa esteja sempre em primeira opção. Contamos com profissionais gerontólogos e enfermeiros que possuem anos de experiência, onde foram absorvidos grandes conhecimentos, para que possam exercer suas funções com grande êxito em nossa empresa de cuidadores de idosos em São Paulo. Um dos nossos maiores objetivos, é melhorarmos a qualidade de vida de nossos pacientes. Portanto, nos adequamos e adaptamos a qualquer situação que nos é posta, para entregarmos o máximo de conforto para tal. Um exemplo, é o fato de disponibilizarmos a possibilidade de troca de funcionários, caso haja uma falta de adaptação de nossos pacientes para com os mesmos. E também, moldamos nossos horários como necessário, para que nossos pacientes possam receber nossos serviços sem nenhum tipo de atrapalho. Nós somos uma empresa de cuidadores de idosos em São Paulo, com serviços completos, como nossas atividades diárias para darem estímulo aos nossos pacientes e também, os relatórios feitos por nossos profissionais, ao final de cada acompanhamento, para que os familiares e responsáveis possam fazer suas análises de como os nossos serviços estão sendo aplicados. Portanto, não deixe de entrar em contato com um de nossos profissionais para nos apresentar suas necessidades e então as correspondermos, através de nossos serviços.</p>

<h2>Mais informações sobre a nossa empresa de cuidadores de idosos em São Paulo</h2>
<p>Visamos sempre entregarmos o melhor atendimento em qualquer serviço prestado por nós, para que sempre que os nossos clientes precisarem de uma empresa de cuidadores de idosos em São Paulo, com profissionalismo e qualidade, os mesmos já saibam que temos todos os recursos e conhecimentos necessários, para que estejamos aptos para atender a qualquer situação que chegar até nós.</p>

<h3>A melhor empresa de cuidadores de idosos em São Paulo</h3>
<p>Queremos sempre fazer com que a nossa empresa de cuidadores de idosos em São Paulo seja sempre acessível a qualquer pessoa que vier a nos consultar. Portanto, deixamos os nossos valores baixos, porém mantendo nossa alta qualidade, para que não só alcancemos a expectativa de todos, mas para que as superemos também.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>