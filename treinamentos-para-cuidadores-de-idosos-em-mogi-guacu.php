<?php
$title       = "Treinamentos para Cuidadores de Idosos em Mogi Guaçu";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Durante o nosso Treinamentos para Cuidadores de Idosos em Mogi Guaçu, você verá como são atribuídos nossos trabalhos, juntamente com os nossos recursos de qualidade. Portanto, para que você tenha conhecimentos exclusivos e somente que a Onix pode te fornecer, entre em contato com um de nossos representantes. Será um prazer contribuirmos para o seu crescimento profissional.</p>
<p>Sendo referência no ramo Cuidado ao Idoso, garante o melhor em Treinamentos para Cuidadores de Idosos em Mogi Guaçu, a empresa Onix Gestão Do Cuidado trabalha com os profissionais mais qualificados do mercado em que atua, com experiências em Acompanhante de Idosos, Cuidador de Idosos 3 Vezes por Semana, Serviço de Acompanhamento de Idosos, Cuidar de Idosos em Casas Particulares e Avaliação Gerontológica Preço para assim atender as reais necessidades de nossos clientes e parceiros. Venha conhecer a qualidade de nosso trabalho e nosso atendimento diferenciado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>