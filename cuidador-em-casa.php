<?php
    $title       = "Cuidador em casa";
    $description = "O serviço de cuidador em casa da Onix, levará a você e ao nosso paciente, o conforto e segurança que o mesmo precisa. Conte sempre com nossos serviços.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para você que está buscando e necessitando de um cuidador em casa que depositara toda sua atenção e cuidado no idoso, encontrou lugar ideal onde todas as suas expectativas serão alcançadas e até mesmo superadas. Sabemos que pessoas de mais idade precisam de uma atenção mais aprofundada do que eu normal e na maioria das vezes os familiares que convivem Com os mesmos não têm uma disponibilidade de tempo onde posso estar focados 100% do tempo nos mesmos. Nós da onix queremos fazer com que nossos pacientes se sintam mais confortáveis possíveis com o nosso cuidador em casa e para isso nós fazemos o que for necessário para que isso ocorra como por exemplo a disponibilidade de troca de funcionários por falta de adaptação E até mesmo a alteração de horário conforme for A rotina de nosso paciente. Vale lembrar que nós fazemos o acompanhamento integral também e damos suporte 24h00 conforme a família e responsáveis acharem necessário. O cuidador em casa auxilia o paciente em atividades diárias para que haja uma possível evolução em seus afazeres do dia dia. E também exerce relatórios a cada fim de dia apresentando os a família e aos responsáveis para que os mesmos possam ter conhecimento de como funciona o nosso trabalho de cuidador em casa. Um dos nossos maiores objetivos é fazer com que nossos pacientes tem uma melhoria de qualidade de vida através de nossos trabalhos e para isso nós contamos com profissionais gerontólogos e enfermeiros diplomatas e experientes em suas áreas para que possamos juntos alcançar tal objetivo. E através das experiências dos mesmos nós estamos sempre utilizando de novas técnicas em cada um de nossos serviços para que possamos nos tornar cada vez mais únicos e referência nesse ramo</p>

<h2>Mais informações sobre o nosso serviço de cuidador em casa</h2>
<p>Como já citado queremos fazer com que mais pessoas possam ter acesso ao nosso serviço de cuidador em casa a cada dia e para isso fazemos com que os valores de nossos serviços sejam acessíveis para que a qualquer momento em que você necessitar de qualquer cuidado para idoso você possa consultar o serviço da onyx.</p>

<h3>A melhor opção de cuidador em casa</h3>
<p>Estamos sempre disponíveis para que você possa entrar em contato com qualquer um de nossos representantes para tirar suas demais dúvidas sobre nosso cuidador em casa. Não deixe de entrar em contato conosco. Aguardamos ansiosamente para melhorarmos a qualidade de vida de quem você ama. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>