<?php
$title       = "Serviço de Cuidadores de Idosos no Aeroporto";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Realize o seu orçamento, até mesmo sem compromisso, o quanto antes perante ao nosso Serviço de Cuidadores de Idosos no Aeroporto, para que você veja em prática toda a nossa teoria. Priorizamos que os nossos valores sejam acessíveis, para que a qualquer momento que qualquer pessoa, precisar de nossos trabalhos, a mesma tenha acesso. Contudo, não adie ainda mais essa oportunidade única.</p>
<p>Na busca por uma empresa referência, quando o assunto é Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Acompanhamento de Idosos em Hospitais, Atenção Domiciliar ao Idoso, Cuidadora de Idosos Particular, Cuidador Hospitalar Preço e Acompanhamento Gerontológico, oferece Serviço de Cuidadores de Idosos no Aeroporto com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>