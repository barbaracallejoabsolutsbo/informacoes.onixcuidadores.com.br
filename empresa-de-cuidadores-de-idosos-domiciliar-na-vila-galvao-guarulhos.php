<?php
$title       = "Empresa de Cuidadores de Idosos Domiciliar na Vila Galvão - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Consultar uma Empresa de Cuidadores de Idosos Domiciliar na Vila Galvão - Guarulhos é extremamente necessário para você que busca levar uma melhoria na qualidade de vida do idoso. Estamos sempre atualizando os métodos utilizados em nosso atendimento, para que possamos não só atender as expectativas de nossos pacientes e sim, superá-las. Estamos disponíveis a qualquer momento.</p>
<p>Líder no segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado dispõe dos melhores e mais modernos recursos do mercado. Nossa empresa trabalha com o objetivo de viabilizar Empresa de Cuidadores de Idosos Domiciliar na Vila Galvão - Guarulhos com a qualidade que você tanto procura. Somos, também, especializados em Cuidar de Idosos em Casas Particulares, Home Care Cuidador de Idosos, Avaliação Gerontológica Preço, Atenção Domiciliar ao Idoso e Acompanhante de Idosos em Hospital, pois, contamos com uma equipe competente e comprometida em prestar um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>