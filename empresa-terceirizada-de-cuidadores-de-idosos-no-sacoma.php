<?php
$title       = "Empresa Terceirizada de Cuidadores de Idosos no Sacomã";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Uma Empresa Terceirizada de Cuidadores de Idosos no Sacomã de qualidade, necessitar ter o conjunto de profissionais experientes e qualificados, para que estejam aptos para auxiliar o paciente no que for necessário. E na Onix, você encontra isso e muito mais. Através de nosso site você pode agendar uma avaliação com um de nossos especialistas e entrar em contato conosco.</p>
<p>Na busca por uma empresa qualificada em Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha com as melhores vantagens para o que você vem buscando. Além de fornecedor Equipe Multiprofissional de Atenção Domiciliar, Agência de Home Care Cuidador, Cuidador Hospitalar Preço, Empresa de Cuidadores de Idosos e Empresa de Cuidador de Idosos com o melhor custo x benefício da região, nós temos como missão garantir agilidade, qualidade e dedicação no que vem realizando para garantir a satisfação de seus clientes que buscam por Empresa Terceirizada de Cuidadores de Idosos no Sacomã.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>