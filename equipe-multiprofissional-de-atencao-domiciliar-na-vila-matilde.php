<?php
$title       = "Equipe Multiprofissional de Atenção Domiciliar na Vila Matilde";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Vale lembrar que a nossa Equipe Multiprofissional de Atenção Domiciliar na Vila Matilde, atende a diversos ambientes além do domicílio. Como por exemplo, clínicas, casas de repousos e hospitais. Portanto, não pense que por conta da sua atual situação, os nossos serviços são inalcançáveis a você. Temos disponibilidades de horários, conforme a rotina do clientes para que 
 possam nos solicitar a qualquer momento.</p>
<p>Se você está em busca de Equipe Multiprofissional de Atenção Domiciliar na Vila Matilde tem preferência por uma empresa com conhecimento e custo-benefício, a Onix Gestão Do Cuidado é a melhor opção para você. Com uma equipe formada por profissionais amplamente qualificados e dedicados para oferecer soluções personalizadas para cada cliente que busca pela excelência. Entre em contato com a gente e saiba mais sobre Cuidado ao Idoso e todos os nossos serviços como Empresa de Acompanhante de Idosos, Serviços de Transporte para Idosos, Atenção Domiciliar ao Idoso, Cuidar de Idosos Fim de Semana e Cuidador de Idosos com Fratura de Fêmur.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>