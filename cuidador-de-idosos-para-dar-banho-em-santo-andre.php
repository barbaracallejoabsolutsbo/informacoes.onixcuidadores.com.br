<?php
$title       = "Cuidador de Idosos para Dar Banho em Santo André";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Além do serviço de Cuidador de Idosos para Dar Banho em Santo André, nós fornecemos serviços similares aos nossos clientes, para que possamos auxilia-los no que for necessário. Nós fornecemos nossos serviços em ambientes como, domicílio, hospitais, casa de repouso e clínicas. Portanto, nos apresente a opção que for mais viável a você, para que possamos te entregar nossos melhores trabalhos.</p>
<p>Especialista no segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado é uma empresa diferenciada, com foco em atender de forma qualificada todos os clientes que buscam por Cuidador de Idosos para Dar Banho em Santo André. Trabalhando com o foco em proporcionar a melhores experiência para seus clientes, nossa empresa conta com um amplo catálogo para você que busca por Acompanhante de Idosos, Cuidadora de Idosos Particular, Equipe Multiprofissional de Atenção Domiciliar, Quanto Custa Cuidadores de Idosos e Serviço de Acompanhamento de Idosos e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>