<?php
$title       = "Empresa de Cuidador de Idosos em Jardim São Paulo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A nossa Empresa de Cuidador de Idosos em Jardim São Paulo se adapta as necessidades de cada cliente, seja em horários ou em nossos serviços. Melhoramos a qualidade de nossos produtos e estrutura a cada ano de atuação, para que independente do que nos for solicitado, nós tenhamos todos os recursos necessários para atender. Não deixe de entrar em contato conosco através dos nossos meios de contatos.</p>
<p>Como uma especialista em Cuidado ao Idoso, a Onix Gestão Do Cuidado se destaca dentre as demais empresas quando o assunto é Empresa de Cuidador de Idosos em Jardim São Paulo, uma vez que, nossa empresa conta com mão de obra com amplo conhecimento em diferentes ramificações do segmento, assim como em Agência de Home Care Cuidador, Acompanhante de Idosos, Empresa de Cuidadores de Idosos, Cuidador de Idosos com Alzheimer e Acompanhamento Hospitalar. Temos os mais competentes profissionais e as principais ferramentas do mercado de modo a prestar o melhor atendimento possível.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>