<?php
    $title       = "Cuidar de idosos fim de semana";
    $description = "Um cuidador de idosos de fim de semana da Onix é extremamente importante para você que têm os dias muito corridos e não consegue dar 100% de atenção aos mesmos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O serviço de cuidar de idosos fim de semana, pode ser necessário constantemente ou até mesmo de uma forma emergencial por motivos pessoais. Mas com a Onix, independente de quando você nos solicitar, nós conseguiremos corresponder às suas necessidades. Até mesmo porque o nosso serviço de cuidar de idosos fim de semana pode ser feito através de um acompanhamento integral ou até mesmo suporte 24h00, conforme for necessário para cada paciente. Todos os profissionais de nossa empresa, possuem longos anos de experiência com cuidados para idosos; portanto, os mesmos estão aptos para qualquer tipo de solicitação. Ao entrar em contato conosco, ficará visível a você, a qualidade e a transparência de nossos trabalhos. Portanto não deixe de garantir o nosso serviço de cuidar de idosos fim de semana, até mesmo para que possamos fazer um acompanhamento do mesmo, melhorando sua qualidade de vida. Nossos profissionais realizarão atividades em que os idosos serão estimulados a evoluírem cada vez mais e suas habilidades, para até mesmo se tornarem independentes em seus afazeres diários. São realizados também, relatórios após cada dia de acompanhamento, durante o final de semana, para que os familiares e responsáveis tenho ciência de como os trabalhos estão sendo aplicados para com os mesmos. Um dos nossos maiores objetivos é levarmos o máximo de conforto e segurança; tanto para quem venha recorrer aos serviços, tanto para quem irá recebê-los. Portanto, nós fazemos o que for necessário para que todos tenham um  bem-estar ao nos consultar. Nós fazemos a alteração de funcionários por falta de adaptação e até mesmo moldamos nossas técnicas para as realizarmos da maneira que nosso paciente desejar. Consulte o quanto antes nosso serviço de cuidar de idosos fim de semana</p>

<h2>Mais informações sobre nosso serviço de cuidar de idosos fim de semana</h2>
<p>Sabemos da qualidade que há em nosso serviço de cuidar de idosos fim de semana e para que todos tenham acesso ao mesmo, deixamos nossos valores extremamente acessíveis. Não perca essa oportunidade</p>
<h3>O melhor lugar para cuidar de idosos fim de semana</h3>
<p>Através de nosso site você pode solicitar o seu orçamento perante o nosso serviço de cuidar de idosos fim de semana. E para facilitar o seu contato conosco, nós possuímos os aplicativo onde você pode agendar uma avaliação e até mesmo falar com um de nossos representantes. Não perca essa oportunidade de fazer com que as suas preocupações seja cessadas e ao mesmo tempo, ajudando a melhorar a qualidade de vida de quem você ama e cuida.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>