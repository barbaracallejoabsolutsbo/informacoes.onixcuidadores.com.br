<?php
$title       = "Empresa de Cuidadores de Idosos no Ibirapuera";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A nossa Empresa de Cuidadores de Idosos no Ibirapuera conta com profissionais extremamente capacitados e experientes nesse ramo, para que possam exercer suas devidas funções em nossa empresa. Estamos sempre utilizando de novas técnicas para que os nossos pacientes tenham o bem-estar que merecem ao nos consultar. Desde seu primeiro contato conosco, você já se torna nossa prioridade.</p>
<p>Além de sermos uma empresa especializada em Empresa de Cuidadores de Idosos no Ibirapuera disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Serviços de Transporte para Idosos, Cuidador de Idosos com Alzheimer, Treinamentos para Cuidadores de Idosos, Home Care Idosos Preço e Alzheimer Cuidados de Enfermagem. Com a ampla experiência que a equipe Onix Gestão Do Cuidado possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Cuidado ao Idoso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>