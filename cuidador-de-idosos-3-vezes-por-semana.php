<?php
    $title       = "Cuidador de idosos 3 vezes por semana";
    $description = "A Onix possui o serviço de cuidador de idosos 3 vezes por semana, especificamente para que o paciente tenha a atenção e cuidado que merecem. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O nosso serviço de cuidador de idosos 3 vezes por semana é apto para você que por motivos pessoais não pode cuidar de seu familiar todos os dias, ou até mesmo para que façamos um acompanhamento individual e personalizado, através de atividades para buscarmos uma evolução no desenvolvimento do mesmo. Vale lembrar que o nosso serviço de cuidador de idosos 3 vezes por semana, é disponibilizado em hospitais, casas de repouso, clínicas e até mesmo domicílio. Contudo, nós lhe forneceremos o que for a melhor opção para você. Garantimos que não só o nosso paciente, mas quem nos constatar, terão as melhores experiências conosco, pois queremos fazer com que vocês sintam o conforto e segurança que queremos passar, desde seu primeiro contato conosco. Até mesmo para que a nossa qualidade seja cada vez mais exclusiva nesse mercado. A cada dia, nossos profissionais aprimoram suas habilidades em nosso serviço de cuidador de idosos 3 vezes por semana, elaborando novas atividades para que os mesmos possam corresponder todas as necessidades que há em nossos pacientes. Os mesmos realizam relatórios diários para apresentarem à família e responsáveis e até mesmo para fazer um acompanhamento melhor da evolução dos pacientes, no que for necessário. Nosso objetivo é melhorarmos a qualidade de vida de nossos pacientes com nosso cuidador de idosos 3 vezes por semana. Entre em contato conosco o quanto antes, para que você possa conhecer nossos serviços de perto. Queremos levar o máximo de conforto em nosso atendimento e para isso, nós fazemos o que for necessário, para que os mesmos se sintam confortáveis quando mantiveram contato com os nossos profissionais.</p>

<h2>Mais informações sobre nosso cuidador de idosos 3 vezes por semana</h2>
<p>Nosso cuidador de idosos 3 vezes por semana, realiza um acompanhamento integral e também da suporte 24h, para que juntos, possamos realizar todos os objetivos da forma mais orgânica possível. Nossos profissionais, tanto gerontólogos, quanto enfermeiros, são altamente capacitados para exercerem suas funções, pelas suas experiências e por serem diplomatas. Não perca essa oportunidade de melhorar a qualidade de vida de quem você ama, de uma forma profissional, cautelosa e com a segurança necessária.</p>

<h3>A melhor opção para cuidador de idosos 3 vezes por semana</h3>
<p>Em nosso site, você pode falar com um de nossos especialistas em nosso site para tirar qualquer dúvida que possui perante ao nosso cuidador de idosos 3 vezes por semana. Aguardamos pelo seu contato com um de nossos profissionais. Conte sempre com os serviços da Onix.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>