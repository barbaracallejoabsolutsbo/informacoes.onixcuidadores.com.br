<?php
    $title       = "Cuidadores de idosos";
    $description = "Os Cuidadores de idosos da Onix, utilizam dos melhores recursos e técnicas para que todos os seus objetivos para conosco sejam alcançados.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Se você está em busca por um lugar que fornece o melhor atendimento de cuidadores de idosos para você e para quem está necessitando, encontrou o lugar ideal para que todas as suas expectativas possam ser alcançadas e correspondidas. Todos os nossos anos trabalhando com Cuidadores de idosos fizeram com que nós absorvêssemos grandes conhecimentos nessa área para que hoje em dia, os nossos serviços estejam cada vez mais em expansão. E ao longo de todos eles, nós aprimoramos e evoluímos nossas técnicas utilizadas em nossos atendimentos, para que estejamos aptos a qualquer situação que chegar até nós. Portanto, não hesite em nos consultar, pois ao entrar em contato com um de nossos cuidadores de idosos, você verá que temos todos os recursos necessários para levar o máximo de conforto aos nossos pacientes. Ao longo de cada acompanhamento, os nossos Cuidadores de idosos fornece atividades para que os idosos possam se tornar cada vez mais independente em seus afazeres diários, auxiliando no que for necessário. E até mesmo para fazer uma possível evolução no que o idoso precisar. E como parte de nossos procedimentos, os nossos profissionais apresentam relatórios diários aos familiares e aos responsáveis para que os mesmos possam ter um melhor acompanhamento, de como os trabalhos estão sendo aplicados. É importante firmarmos que qualquer um de nossos serviços podem ser postos e prestados em diversos tipos de ambientes, como por exemplo hospitais, clínicas, casa de repouso e até mesmo no próprio domicílio. Contudo, não adie cada vez mais essa oportunidade de adquirir os serviços da melhor empresa que realiza cuidados para idosos.</p>

<h2>Mais informações sobre nossos cuidadores de idosos</h2>
<p>Sabemos da importância que há na adaptação de nossos idosos para com os nossos cuidadores e através disso, nós disponibilizamos uma possível troca de nossos funcionários caso tal feito não ocorra. Pois nossa prioridade é fazer com que nossos pacientes se sintam mais confortáveis com qualquer um de nossos cuidadores de idosos. Nós disponibilizamos também o acompanhamento integral e o suporte 24h00 conforme for as necessidades de cada paciente.</p>
<h3>O melhor lugar para adquirir cuidadores de idosos</h3>
<p>Cada um de nossos cuidadores de idosos sempre fazem o que for possível para que haja uma melhora na qualidade de vida de nossos pacientes. E para que você garanta isso a pessoa que você cuida, entre em contato com um de nossos especialistas até mesmo através de nosso site. Aguardamos ansiosamente pelo seu contato.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>