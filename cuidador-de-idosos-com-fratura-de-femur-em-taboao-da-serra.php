<?php
$title       = "Cuidador de Idosos com Fratura de Fêmur em Taboão da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>É necessário consultar um serviço de Cuidador de Idosos com Fratura de Fêmur em Taboão da Serra, pois os profissionais qualificados prestarão todos os serviços que são necessários para que haja uma constante melhora e também, para que o idoso consiga realizar suas atividades diárias sem nenhum empecilho. Visamos melhorar a qualidade de vida de nossos pacientes, para que os mesmos sejam cada vez mais independentes.
</p>
<p>Além de sermos uma empresa especializada em Cuidador de Idosos com Fratura de Fêmur em Taboão da Serra disponibilizamos uma equipe de profissionais altamente competente a fim de prestar um ótimo atendimento em Atenção Domiciliar ao Idoso, Acompanhamento Hospitalar para Idoso, Serviço de Acompanhamento de Idosos, Cuidador de Idosos Fim de Semana e Avaliação Gerontológica Preço. Com a ampla experiência que a equipe Onix Gestão Do Cuidado possui na atualidade, garantimos um constante desenvolvimento voltado a melhorar ainda mais nos destacados entre as principais empresas do mercado de Cuidado ao Idoso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>