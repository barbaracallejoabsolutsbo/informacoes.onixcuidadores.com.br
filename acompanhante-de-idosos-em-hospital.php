<?php
    $title       = "Acompanhante de idosos em hospital";
    $description = "Nosso acompanhante de idosos em hospital, têm os melhores recursos para que possamos melhorar a qualidade de vida de quem necessita. Entre em contato com a onix";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Onix fornece o serviço de acompanhante de idosos em hospital, para aqueles que necessitam de um cuidado e atenção a mais, pois possuem algum tipo de vulnerabilidade. Nosso acompanhante de idosos em hospital, realiza relatórios diários para apresentar à família e responsáveis e então mostrarem possíveis evoluções através das atividades fornecidas pelos mesmos. Nós realizamos também um acompanhamento integral ou um suporte 24h, conforme as necessidades de nossos clientes. É importante ressaltar que temos acompanhante de idosos em hospital e em outros lugares, como em casa de repouso e até mesmo em domicílio. Portanto, consulte um de nossos profissionais para obter o serviço que é mais viável a você. O serviço de acompanhante de idosos em hospital, é um serviço individual e personalizado pois cada paciente possui as suas necessidades. Nossos profissionais são altamente capacitados e qualificados para exercerem suas funções pois só executam as mesmas pelos seus longos anos de estudos nessa área. E a cada experiência, os mesmos atribuem mais conhecimentos para aplicarem em cada atendimento, contudo, melhorando a qualidade de nossos serviços cada vez mais, até mesmo para que possamos recorrer a qualquer tipo de necessidade. Nosso valor, é garantirmos as necessidades básicas para os idosos em qualquer instituição, para que possamos realizar o conjunto de nossos trabalhos. Queremos a cada dia, fazer com que nossos serviços e atendimentos sejam mais únicos nesse ramo e para isso, possuímos os melhores recursos e técnicas, para que sejamos exclusivos. Entregamos sempre nosso melhor atendimento, para que sempre que você necessitar desse ou demais serviços que possuímos, nós sejamos a sua primeira opção. Fazemos sempre o nosso melhor, para que não só nós tenhamos benefícios, mas para que realmente possamos mudar vidas através de nossos trabahos.</p>

<h2>Mais informações sobre nosso acompanhante de idosos em hospital</h2>
<p>Estamos disponíveis a todo momento para que você possa adquirir nosso acompanhante de idosos em hospital. Garantimos que conquistaremos a sua confiança desde o nosso primeiro contato. E também, fazemos o que for possível, para garantirmos o seu conforto. Agende a sua avaliação em nosso site, para podermos colocar os seus planos e objetivos em prática, o quanto antes.</p>

<h3>A melhor opção de acompanhante de idosos em hospital</h3>
<p>Em nosso site, você poderá falar com um de nossos especialistas e até mesmo solicitar o seu orçamento. Acompanhe nossas redes sociais também, para saber mais detalhadamente sobre nosso serviço de acompanhante de idosos em hospital. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>