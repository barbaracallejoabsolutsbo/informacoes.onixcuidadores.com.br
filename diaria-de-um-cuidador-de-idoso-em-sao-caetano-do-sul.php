<?php
$title       = "Diária de um Cuidador de Idoso em São Caetano do Sul";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Com a Diária de um Cuidador de Idoso em São Caetano do Suls, o idoso terá todo o cuidado e auxílio necessário, com um acompanhamento integral ou suporte  24h. Portanto, para garantir uma melhoria na qualidade de vida de seu familiar, agenda uma avaliação com um de nossos especialistas através de nosso site, para que nossos profissionais lhe apresentem nossos melhores trabalhos.</p>
<p>Procurando uma empresa de confiança onde você possa ter a garantia de satisfação em Diária de um Cuidador de Idoso em São Caetano do Sul? A Onix Gestão Do Cuidado é o que você precisa! Sendo uma das principais empresas do ramo de Cuidado ao Idoso consegue proporcionar o melhor para seus clientes e parceiros, uma vez que, é especializada em Home Care Cuidador de Idosos, Empresa de Cuidadores de Idosos, Empresa de Cuidadores de Idosos Domiciliar, Serviço de Cuidadores de Idosos e Acompanhante de Idosos. Entre em contato e fique por dentro de mais informações.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>