<?php
$title       = "Cuidador de Idosos Hospitalar em Valinhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nosso Cuidador de Idosos Hospitalar em Valinhos, acompanha nossos pacientes no que for necessário, seja com suporte 24h ou com um acompanhamento integral. Nossos profissionais gerontólogos e enfermeiros entregam aos familiares e responsáveis, relatórios ao fim de cada de de acompanhamento, para que todos possam ter ciência de como os trabalhos da Onix, são aplicados. </p>
<p>Com a Onix Gestão Do Cuidado proporcionando de forma excelente Treinamentos para Cuidadores de Idosos, Cuidador de Idosos Fim de Semana, Acompanhante de Idosos, Cuidador de Idosos 3 Vezes por Semana e Empresa Terceirizada de Cuidadores de Idosos conseguindo manter a alta qualidade e credibilidade no ramo de Cuidado ao Idoso, assim, consequentemente, proporcionando o que se tem de melhor em resultados para você. Possibilitando diversas escolhas para os melhores resultados, a nossa empresa torna-se referência com Cuidador de Idosos Hospitalar em Valinhos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>