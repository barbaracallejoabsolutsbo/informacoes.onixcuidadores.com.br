<?php
$title       = "Empresa Terceirizada de Cuidadores de Idosos em Cabuçu de Cima - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Somos uma Empresa Terceirizada de Cuidadores de Idosos em Cabuçu de Cima - Guarulhos com serviços completos e com os melhores recursos, para que nos tornemos cada vez mais referência nesse ramo. Em cada acompanhamento, nós usufruímos de técnicas exclusivas para que a família, responsáveis e até mesmo o paciente, se surpreenda com a nossa qualidade. Não deixe de consultar nossos serviços.</p>
<p>Tendo como especialidade Cuidadora de Idosos Particular, Empresa de Cuidadores de Idosos, Acompanhante de Idosos em Hospital, Gerontologia Cuidado ao Idoso e Empresa Terceirizada de Cuidadores de Idosos, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Cuidado ao Idoso. Por isso, quando falamos de Empresa Terceirizada de Cuidadores de Idosos em Cabuçu de Cima - Guarulhos, buscar pelos membros da empresa Onix Gestão Do Cuidado é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>