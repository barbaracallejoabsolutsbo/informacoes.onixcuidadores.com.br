<?php
$title       = "Cuidador de Idosos Fim de Semana no Alto da Lapa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O nosso serviço de Cuidador de Idosos Fim de Semana no Alto da Lapa, pode ser solicitado quando a família ou responsáveis necessitam realizar algum compromisso, ou seja, de forma emergencial, mas pode ser solicitado também, de forma planejada e em demais finais de semana. Portanto, consulte um de nossos profissionais através de nosso site ou números de telefone, para te auxiliar no que for necessário.</p>
<p>Com uma ampla atuação no segmento, a Onix Gestão Do Cuidado oferece o melhor quando falamos de Cuidador de Idosos Fim de Semana no Alto da Lapa proporcionando aos seus clientes a máxima qualidade e desempenho em Cuidador de Idosos Fim de Semana, Empresa de Acompanhante de Idosos, Cuidadora de Idosos Particular, Cuidador de Idosos Hospitalar e Acompanhamento Hospitalar, uma vez que, a nossa equipe de profissionais que atuam para proporcionar aos clientes sempre o melhor. Somos a empresa que mais se destaca quando se trata de Cuidado ao Idoso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>