<?php
    $title       = "Serviços de transporte para idosos";
    $description = "Os nossos serviços de transporte para idosos estão disponíveis a todo momento em que você precisar. A Onix está localizada em São Paulo, nos faça uma visita!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Os nossos serviços de transporte para idosos, são serviços completos, onde são utilizados métodos exclusivos e os melhores recursos, para que possamos atender a todas as necessidades de nossos pacientes. Os nossos serviços de transporte para idosos serve para que ajudemos os idosos no que for necessário, para que tenham uma evolução no que desejam. Seja através de atividades e estímulos que nossos profissionais realizam com os nossos pacientes, para auxiliarmos no que for preciso. Em nossos serviços de transporte para idosos, contamos com profissionais gerontólogos e enfermeiros extremamente capacitados, pois possuem longos anos de experiência e estudos, para que possam exercer suas atividades dentro de nossa empresa. Os nossos serviços de transporte para idosos, podem ser atendidos em domicílios, casas de repouso, hospitais e clínicas. Portanto, consulte um de nossos profissionais, para nos apresentar a opção que for mais viável a você. Nosso objetivo é melhorarmos a qualidade de vida de nossos pacientes, através das atividades que são realizadas para criar estímulos do bem-estar dos mesmos. Na Onix, nós disponibilizamos a troca de funcionários caso haja uma falta de adaptação de nossos pacientes para com os mesmos. Nós também oferecemos a troca de nossos horários de atendimento, conforme for a rotina de cada paciente. Entregamos sempre nossos melhores serviços, desde o seu mínimo contato conosco, para que a qualquer momento em que você precisar de qualquer serviço para idoso, já saiba que somos o único lugar é apto para corresponder as suas necessidades. Até mesmo para que nos tornemos cada vez mais referência e exemplo dentro desse mercado.</p>

<h2>Mais detalhes sobre nossos serviços de transporte para idosos</h2>
<p>Sabemos da importância que há nos serviços de transporte para idosos e para isso, deixamos os valores dos mesmos extremamente acessíveis, para que ao nos consultar, você não tenha nenhum prejuízo financeiro. Desde o nosso princípio, nós mantemos o nosso preço baixo, porém com uma alta qualidade. Até mesmo para que mais pessoas possam ter acesso ao nosso serviço de excelência. Em nosso site você pode realizar o seu orçamento de forma prática, mas caso prefira, nossos profissionais estão sempre disponíveis através dos nossos meios de contatos.</p>
<h3>A melhor opção de serviços de transporte para idosos</h3>
<p>Em nosso site há vários meios de contatos para que você possa falar com um de nossos especialistas. E também, temos o nosso aplicativo, onde você possa agendar uma avaliação perante aos nossos serviços de transporte para idosos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>