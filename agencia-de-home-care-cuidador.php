<?php
    $title       = "Agência de home care cuidador";
    $description = "A agência de home care cuidador da Onix, é a melhor opção para você que deseja ter uma melhoria em sua qualidade de vida, sem sair de casa. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>A Onix, é o lugar ideal para você que busca por uma agência de home care cuidador, pois nós possuímos profissionais excelentes e extremamente capacitados para exercerem suas funções com grande êxito. Além de que nós oferecemos um baixo custo, para que todos os que necessitam de uma agência de home care cuidador, possam ter acesso a mesma. Nós somos uma empresa que disponibiliza diversos serviços de cuidador, pois sabemos a necessidade de cuidado que alguns idosos possuem. E para que também, a família e responsáveis, possam ter a segurança e conforto que desejam. Uma agência de home care cuidador fornece serviço a domicílio, mas em nossa empresa, temos serviços de cuidador em casa de repouso, hospital e demais lugares. Portanto, nos consulte para que possamos prestar nosso melhor serviço a você. Buscamos sempre inovarmos em nossa agência de home care cuidador, para nos adaptarmos ao que for necessário, para que nossos pacientes possam ter todas as suas correspondidas por nós. Até mesmo para que sejamos cada vez mais referência nesse ramo, pela qualidade de nossos serviços e atendimento. Como já citado, nossos profissionais possuem grandes experiências, com seus anos atuando no ramo de cuidadores de idosos. Consequentemente, dão o seu melhor em cada processo de atendimento, desde o seu primeiro contato, para que você e nós, tenhamos os objetivos atendidos. A Onix está há anos aprimorando cada vez mais seus serviços de cuidador, para que possamos surpreender cada vez mais com a nossa qualidade e fazendo com que todos tenham acessibilidade aos mesmos. Nossos gerontólogos e enfermeiros, exercem atividades aos nossos pacientes, acompanhando-os nas mesmas e fazendo relatórios diários, para acompanharem suas evoluções diárias.</p>

<h2>Mais detalhes sobre a nossa agência de home care cuidador</h2>
<p>Em nossa agência de home care cuidador, as necessidades de nossos pacientes são nossas prioridades. E para isso, nós fazemos o possível para que os mesmos se adaptem aos nossos profissionais; alterando o horário, ou até mesmo realizando a troca dos nossos especialistas, caso haja uma falta de adaptação dos nossos pacientes para com eles. Portanto, não perca essa oportunidade de leva o cuidado que quem você tanto ama, merece.</p>

<h3>A melhor opção de agência de home care cuidador</h3>
<p>Nós estamos localizados em São Paulo, para que você possa vir conhecer nossa agência de home care cuidador, mas caso prefira, em nosso site você pode entrar em contato diretamente com um de nossos especialistas para tirar qualquer dúvida que possuir.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>