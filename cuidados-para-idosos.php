<?php
    $title       = "Cuidados para idosos";
    $description = "Os serviços de cuidados para idosos, da Onix, podem ser prestados em diversos lugares. Consulte um de nossos profissionais e veja o melhor para você.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Desde o seu fundamento, a Onix busca sempre expandir o seus trabalhos para que possa se tornar cada vez mais referência em cuidados para idosos. E também para fazer com que mais pessoas possam ter acesso aos nossos serviços de qualidade. Nós estamos há longos anos fornecendo diversos cuidados para idosos com profissionais gerontólogos e enfermeiros, que são diplomatas para exercerem suas funções dentro de nossa empresa com grande êxito. Vale lembrar que os nossos cuidados para idosos podem ser postos em qualquer ambiente que for melhor para o idoso. Como por exemplo, em domicílios, hospitais onde os mesmos se encontram em uma situação vulnerável, casa de repouso e até mesmo em clínicas. Contudo, consulte um de nossos profissionais para adquirir a opção que for mais viável para você. Em nossos cuidados para idosos são realizadas atividades diárias onde os nossos profissionais auxiliam os mesmos no que for necessário, para que criem cada vez mais independência em seus afazeres. Os mesmos realizam também relatórios para que os familiares e responsáveis que cuidam do idoso, possam ter um acompanhamento melhor da evolução de nossos pacientes e de com os nossos serviços estão sendo aplicados para com os mesmos. Nós mudamos o que for necessário para que atendemos todos os pedidos e necessidades de nossos pacientes. Como por exemplo, nós damos suporte 24h00 e ou acompanhamento integral. Fale o quanto antes com os nossos profissionais, para que os mesmos possam te auxiliar na escolha de nossos serviços. Nós também disponibilizamos a possibilidade de troca de funcionários sem nenhum prejuízo ao paciente e a família por conta de falta de adaptação do idoso para com o mesmo.</p>

<h2>Mais informações sobre nossos cuidados para idosos</h2>
<p>Nós possuímos um aplicativo exclusivo de nossa empresa, para facilitar o seu acesso até nós. Navegue em nosso site e agende uma avaliação ou até mesmo realize um orçamento de forma prática, perante ao nossos cuidados para idosos. Será um prazer podermos levar nossos serviços com extrema qualidade a você e a quem você ama.</p>

<h3>A melhor opção de cuidados para idosos</h3>
<p>Consulte os nossos trabalhos para você ver que toda nossa teoria é posta em prática; e também para que possamos realizar o nosso objetivo de melhorar a qualidade de mais vidas, com os nossos cuidados para idosos. Conte sempre com todos os serviços da Onix. Estamos sempre dispostos para conversarmos com nossos clientes e pacientes. Conte sempre conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>