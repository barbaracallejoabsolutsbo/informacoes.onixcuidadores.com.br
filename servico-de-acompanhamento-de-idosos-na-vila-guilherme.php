<?php
$title       = "Serviço de Acompanhamento de Idosos na Vila Guilherme";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Todos os profissionais que realizam o Serviço de Acompanhamento de Idosos na Vila Guilherme da Onix, possuem grandes experiências nesse ramo, para que estejam aptos a exercerem suas funções dentro de nossa empresa. Além de que são diplomatas e por consequência possuem um conhecimentos vasto também no mesmo. Estamos em constante evolução para que possamos atender cada vez mais as expectativas de nossos pacientes.</p>
<p>Na busca por uma empresa referência, quando o assunto é Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Cuidadora de Idosos Preço, Acompanhamento Gerontológico, Acompanhante de Idosos, Cuidador de Idosos em Casa e Acompanhamento de Idosos em Hospitais, oferece Serviço de Acompanhamento de Idosos na Vila Guilherme com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>