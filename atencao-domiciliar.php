<?php
    $title       = "Atenção domiciliar";
    $description = "A atenção domiciliar da Onix, levará grandes benefícios para a sua saúde física e mental. Será um prazer te ajudarmos nesse momento de sua vida. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p><br />A Onix possui as melhores condições de tratamento e recursos, quando o assunto é atenção domiciliar. Sabemos que uns pacientes necessitam de mais cuidado do que outros e para isso, nós temos uma vasta disponibilidade para realizarmos um acompanhamento integral e suporte 24h. Portanto, consulte um de nossos profissionais, para que possamos fazer o que mais for viável a você, em nossa atenção domiciliar. Para você que está em busca de uma atenção domiciliar que atenderá todos os seus objetivos, correspondendo suas necessidades e mantendo um baixo custo, encontro o lugar certo na Onix, para que possamos realizar tal. A cada dia, queremos expandir nossos conhecimentos, para que possamos evoluir cada vez mais, fazendo com que a qualidade de nossa atenção domiciliar seja única. Prestamos sempre nossos melhores serviços, em todos os que são prestados por nós, para que possamos nos tornar referência em cuidados para idosos. Nós sabemos da nossa qualidade e por isso, queremos fazer com que mais pessoas tenham acesso a mesma. Todos os nossos profissionais se atentam aos seus pedidos passados desde o nosso primeiro contato, para que possamos obter o resultado que espera o quanto antes. Os mesmos são altamente qualificados para estarem aptos a qualquer tipo de solicitação, portanto não hesite em entrar em contato conosco para obter nossos serviços e também, ter uma melhor qualidade de vida. Queremos que nossos pacientes sintam o máximo de conforto ao presenciar esse nosso serviço e para isso, nos moldamos no que for necessário, para que isso ocorra. Vale lembrar que nós fornecemos nossos trabalhos em domicílio, clínicas, hospitais e casas de repouso. Portanto, independente de onde deseja, você conseguirá obter os serviços que disponibilizamos. Vale ressaltar que nossos profissionais realizam relatórios diários, para que os familiares e responsáveis possam fazer o devido acompanhamento.</p>

<h2>Mais detalhes sobre nossa atenção domiciliar</h2>
<p>Navegue em nosso site, para que você possa realizar o seu orçamento perante a nossa atenção domiciliar ou até mesmo para falar com um de nossos especialistas. Estamos há longos anos sempre fornecendo nosso melhor trabalho, com um baixo custo, para que todos possam ter acessibilidades aos mesmos.</p>

<h3>O melhor lugar para adquirir uma atenção domiciliar</h3>
<p>Nossos profissionais estão dispostos para tirarem qualquer dúvida que possuir perante a nossa atenção domiciliar. Em nosso site há diversos meios de contato para que você possa falar conosco, ou até mesmo através de nosso site. Aguardamos ansiosamente pelo seu contato, para juntos melhorarmos a qualidade de sua vida.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>