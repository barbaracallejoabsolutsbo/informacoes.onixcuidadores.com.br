<?php
$title       = "Cuidador de Idosos Fim de Semana na Raposo Tavares";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O nosso serviço de Cuidador de Idosos Fim de Semana na Raposo Tavares, pode ser solicitado quando a família ou responsáveis necessitam realizar algum compromisso, ou seja, de forma emergencial, mas pode ser solicitado também, de forma planejada e em demais finais de semana. Portanto, consulte um de nossos profissionais através de nosso site ou números de telefone, para te auxiliar no que for necessário.</p>
<p>Com credibilidade no mercado de Cuidado ao Idoso, a Onix Gestão Do Cuidado trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Cuidador de Idosos com Alzheimer, Empresa Terceirizada de Cuidadores de Idosos, Acompanhamento de Idosos em Hospitais, Serviço de Acompanhamento de Idosos e Agência de Home Care Cuidador quanto em Cuidador de Idosos Fim de Semana na Raposo Tavares. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>