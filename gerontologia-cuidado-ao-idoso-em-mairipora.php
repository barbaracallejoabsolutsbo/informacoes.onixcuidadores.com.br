<?php
$title       = "Gerontologia Cuidado ao Idoso em Mairiporã";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nosso serviço de Gerontologia Cuidado ao Idoso em Mairiporã, nossos profissionais realizam atividades diárias com os nossos pacientes, para que os mesmos tenham constante evoluções em seus quadros e possam realizar seus afazeres diários sem nenhum empecilho o quanto antes. Nos adaptamos a rotina de nossos pacientes, moldando sempre o que for necessário para garantir o bem-estar dos mesmos.</p>
<p>A Onix Gestão Do Cuidado além de ser uma das principais empresas do setor de Cuidado ao Idoso tem como foco trazer o que se tem de melhor nesse ramo. E por ser uma excelente empresa, se dispõe a prestar uma ótima assessoria, tanto para Gerontologia Cuidado ao Idoso em Mairiporã, quanto para Serviço de Cuidadores de Idosos, Cuidar de Idosos em Casas Particulares, Acompanhamento Gerontológico, Acompanhamento Hospitalar para Idoso e Serviço de Acompanhamento de Idosos. Conte com a gente, pois temos uma equipe de sucesso esperando pelo seu contato.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>