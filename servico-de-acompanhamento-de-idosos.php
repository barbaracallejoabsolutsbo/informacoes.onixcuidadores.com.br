<?php
    $title       = "Serviço de acompanhamento de idosos";
    $description = "Em nosso serviço de acompanhamento de idosos, são utilizados métodos exclusivos para que possamos entregar um trabalho excelente. A Onix aguarda por você.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    

<p>No serviço de acompanhamento de idosos da Onix, A todos os recursos necessários para atender a qualquer tipo de necessidade. Pois fazemos o possível para que todos os nossos pacientes se sintam confortáveis durante o nosso acompanhamento. Até mesmo para que nos tornemos cada vez mais referência com o nosso serviço de acompanhamento de idosos. Nós mudamos o que for necessário para que todos os nossos serviços se apliquem a rotina de nossos pacientes. Como por exemplo a nossa alteração de horário conforme for solicitado e também a nossa disponibilidade de troca de nossos funcionários por falta de adaptação dos nossos pacientes para com os mesmos. Durante o nosso serviço de acompanhamento de idosos os nossos profissionais exercem atividades diárias auxiliando os nossos pacientes no que for necessário para que haja uma evolução no que é preciso. Também são feito os relatórios ao final de cada acompanhamento para que a família e os responsáveis possam ter mais detalhes de como nosso serviço de acompanhamento de idosos funciona. Estamos sempre estudando novas técnicas e as mais atualizadas, para que tenhamos a cada dia, serviços mais únicos. Sabemos da importância que a receber um bom atendimento principalmente nesse serviço portanto desde o seu primeiro contato conosco Nós nos dedicando 100% ao nosso paciente para que sempre que o mesmo precisar desse e demais serviços pensei na Onix como primeira opção. Nós contamos com profissionais gerontologos e enfermeiros que possuem longos anos de experiência nesse ramo, para que possam atuar dentro da nossa empresa com grande êxito. Até mesmo para que estejam preparados a qualquer tipo de situação que chegar até nós.</p>

<h2>Mais detalhes sobre nosso serviço de acompanhamento de idosos</h2>
<p>O serviço de acompanhamento de idosos é extremamente importante para os familiares e responsáveis que por motivos pessoais, acabam não conseguindo depositar 100% de atenção a tal idoso. E para que os nossos serviços sejam cada vez mais acessíveis, nós fazemos com que todos eles tenham um preço baixo, porém com uma alta qualidade. Entre em nosso site e faça o seu orçamento de forma online e extremamente rápida, para garantir o quanto antes nosso serviço mais viável a você.</p>

<h3>O melhor serviço de acompanhamento de idosos</h3>
<p>Fale com um de nossos representantes através de nossos sites, para tirar qualquer dúvida que você possuir perante ao nosso serviço de acompanhamento de idosos. Conte sempre com o atendimento e serviço da Onix. Aguardamos ansiosamente pelo seu contato.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>