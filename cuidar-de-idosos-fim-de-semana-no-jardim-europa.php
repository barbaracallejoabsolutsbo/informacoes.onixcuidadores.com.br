<?php
$title       = "Cuidar de Idosos Fim de Semana no Jardim Europa";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Cuidar de Idosos Fim de Semana no Jardim Europa é extremamente essencial para você que por demais compromissos, acaba não conseguindo ficar com o seu familiar ou dependente o final de semana todo. A Onix conta com profissionais extremamente qualificados e experientes para que estejam aptos a corresponder a qualquer tipo de necessidade, auxiliando no que for necessário.</p>
<p>Se está procurando por Cuidar de Idosos Fim de Semana no Jardim Europa e prioriza empresas idôneas e com os melhores profissionais para o seu atendimento, a Onix Gestão Do Cuidado é a melhor opção do mercado. Unindo profissionais com alto nível de experiência no segmento de Cuidado ao Idoso conseguem oferecer soluções diferenciadas para garantir o objetivo de cada cliente quando falamos de Avaliação Gerontológica Preço, Diária de um Cuidador de Idoso, Agência de Home Care Cuidador, Serviço de Acompanhamento de Idosos e Cuidador de Idosos em Casa.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>