<?php
$title       = "Agência de Home Care Cuidador em Vinhedo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A nossa Agência de Home Care Cuidador em Vinhedo, faz o que for necessário, para que nossos pacientes se sintam confortável durante qualquer tipo de acompanhamento. Todavia, moldamos o que for necessário em nossa agenda ou no quadro de nossos funcionários, para que possamos levar um bem-estar aos mesmos. Desde seu primeiro contato conosco, as suas necessidades tornam-se nossas prioridades.</p>
<p>Trabalhando para garantir Agência de Home Care Cuidador em Vinhedo de maior qualidade no segmento de Cuidado ao Idoso, contar com a Onix Gestão Do Cuidado que, proporciona, também, Acompanhamento Gerontológico, Cuidador de Idosos 3 Vezes por Semana, Cuidador de Idosos com Fratura de Fêmur, Empresa de Cuidador de Idosos e Acompanhante de Idosos com a mesma excelência e dedicação se torna a melhor decisão para você. Se destacando de forma positiva das demais empresas, nós contamos com profissionais amplamente capacitados para um atendimento de sucesso.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>