<?php
$title       = "Acompanhante de Idosos em Hospital na Cachoeirinha";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Um Acompanhante de Idosos em Hospital na Cachoeirinha, é extremamente importante para aqueles pacientes que necessitam de total atenção durante todo o período que estão internados. Nossos profissionais, gerontólogos e enfermeiros realizam todas as atividades que são necessárias, para que o paciente tenha uma evolução no quadro que precisa e deseja. </p>
<p>Na busca por uma empresa referência, quando o assunto é Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Empresa de Acompanhante de Idosos, Cuidador de Idosos com Fratura de Fêmur, Gerontologia Cuidado ao Idoso, Empresa de Cuidador de Idosos e Cuidar de Idosos Fim de Semana, oferece Acompanhante de Idosos em Hospital na Cachoeirinha com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>