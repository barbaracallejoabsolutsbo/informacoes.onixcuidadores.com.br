<?php
$title       = "Acompanhamento de Idosos em Hospitais em Mogi Mirim";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Acompanhamento de Idosos em Hospitais em Mogi Mirim, da Onix, é uma ótima opção a você que pela correria do dia a dia ou outros fatores, não consegue entregar sua atenção o tempo todo ao idoso. Portanto, estamos aqui para te fornecer todo o suporte necessário, com profissionais qualificados e capacitados, onde você não terá receio nenhum ao depositar sua confiança. </p>
<p>Procurando por uma empresa de confiança onde você tenha profissionais qualificados com o intuito de suprir suas necessidades. A empresa Onix Gestão Do Cuidado ganha destaque quando o assunto é Cuidado ao Idoso. Ainda, contamos com uma equipe qualificada para atender suas necessidades, seja quando se trata de Acompanhamento de Idosos em Hospitais em Mogi Mirim até Acompanhamento Hospitalar, Cuidador Hospitalar Preço, Acompanhamento Gerontológico, Empresa de Cuidadores de Idosos e Alzheimer Cuidados de Enfermagem com muita excelência.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>