<?php
$title       = "Empresa de Cuidador de Idosos em Cidade Aracília - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Um dos nossos maiores objetivos é fazermos com que nossos pacientes tenham o máximo de conforto e bem-estar durante qualquer serviço prestado pela nossa Empresa de Cuidador de Idosos em Cidade Aracília - Guarulhos. E para isso, nós moldamos o que for necessário para que isso ocorra. Como por exemplo, a disponibilidade da troca de funcionários por falta de adaptação de nossos pacientes com os mesmos e demais fatores.</p>
<p>A Onix Gestão Do Cuidado além de ser uma das principais empresas do setor de Cuidado ao Idoso tem como foco trazer o que se tem de melhor nesse ramo. E por ser uma excelente empresa, se dispõe a prestar uma ótima assessoria, tanto para Empresa de Cuidador de Idosos em Cidade Aracília - Guarulhos, quanto para Cuidador de Idosos 3 Vezes por Semana, Acompanhante de Idosos em Hospital, Equipe Multiprofissional de Atenção Domiciliar, Cuidador de Idosos Fim de Semana e Cuidador de Idosos em Casa. Conte com a gente, pois temos uma equipe de sucesso esperando pelo seu contato.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>