<?php
    $title       = "Cuidadores de idosos em SP";
    $description = "Nossos cuidadores de idosos em SP estão sempre disponíveis para prestarem seus melhores serviços a você. Conte sempre com os profissionais da Onix.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Na Ônix, você encontra serviço de cuidadores de idosos em SP completos e que correspondem a quaisquer necessidades que você e o nosso paciente possuir. Somos o lugar ideal para que o seus objetivos possam ser atendidos. Nós fornecemos o serviço de cuidadores de idosos em SP, há muitos anos. E dentro das atividades prestadas pelos nossos cuidadores de idosos em SP, são sempre apresentadas novas condições e um custo benefício. Ao consultar um de nossos profissionais você verá que somente na Ônix, você poderá depositar toda a sua confiança, pois nós mostramos toda nossa teoria em prática. Um dos nossos maiores objetivos é fazermos com que a cada dia, mais pessoas possam ter acesso aos nossos serviços de qualidade. E até mesmo para que possamos nos tornar referência nesse ramo, para que quando pensarem em cuidadores de idosos em SP, pensem na Onix. Nossos profissionais são altamente capacitados para te auxiliar no que for necessário antes, durante e após o atendimento dos mesmos com o idoso. Sejam os nossos profissionais gerontologos ou enfermeiros, os mesmos possuem diversos conhecimentos para que pudessem exercer suas atividades dentro da empresa e consequentemente são altamente capacitados e qualificados para recorrerem a qualquer tipo de situação. Por mais que nossos profissionais sejam excelentes, Pode Acontecer de um idoso não se adaptar com o mesmo durante o acompanhamento E como os nossos objetivos elevarmos o máximo de conforto aos mesmos, nós fazemos a disponibilidade de troca dos mesmos, caso seja necessário. É importante lembrar que esse e demais serviços que são fornecidos pela nossa empresa, podem ser aplicados em diversos ambientes como hospitais, casas de repouso, clínicas e até mesmo em domicílio. Portanto consulte um de nossos profissionais o quanto antes, para ver qual é a melhor opção pra você.</p>

<h2>Conheça mais sobre nossos Cuidadores de idosos em SP</h2>
<p>Os cuidadores de idosos em SP da Onix realizam acompanhamento integral e suporte 24h00 conforme for as necessidades de nossos pacientes. São sempre realizados atividades e relatórios após as mesmas, para que possamos acompanhar a evolução de nossos pacientes e até mesmo para que os familiares e responsáveis possam acompanhar com mais detalhes a atuação de nossos serviços.</p>

<h3>A melhor opção de Cuidadores de idosos em SP</h3>
<p>Agende uma avaliação em nosso site com nossos profissionais para obter nosso serviço de cuidadores de idosos em SP e garantir uma melhor qualidade de vida à pessoa que você ama e cuida.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>