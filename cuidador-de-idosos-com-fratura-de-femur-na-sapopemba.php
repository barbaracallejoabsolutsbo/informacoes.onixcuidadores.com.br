<?php
$title       = "Cuidador de Idosos com Fratura de Fêmur na Sapopemba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nós fornecemos o nosso serviço de Cuidador de Idosos com Fratura de Fêmur na Sapopemba em ambientes como, casa de repouso, hospitais, domicílios e clínicas. Portanto, entre em contato com um de nossos profissionais e nos apresente suas necessidades para que possamos saná-las o quanto antes. Todos os nossos meios de contatos estão sempre disponíveis para que você possa nos consultar.</p>
<p>Tendo como especialidade Home Care Cuidador de Idosos, Serviço de Cuidadores de Idosos, Agência de Home Care Cuidador, Cuidador de Idosos Fim de Semana e Acompanhante de Idosos, nossos profissionais possuem ampla experiência e conhecimento avançado no segmento de Cuidado ao Idoso. Por isso, quando falamos de Cuidador de Idosos com Fratura de Fêmur na Sapopemba, buscar pelos membros da empresa Onix Gestão Do Cuidado é a melhor forma para alcançar seus objetivos de forma rápida e garantida. Entre em contato. Nós podemos te ajudar.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>