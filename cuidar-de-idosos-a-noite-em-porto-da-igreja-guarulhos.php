<?php
$title       = "Cuidar de Idosos a Noite em Porto da Igreja - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Os idosos necessitam de uma atenção maior durante todo o dia, mas ao Cuidar de Idosos a Noite em Porto da Igreja - Guarulhos, você obterá mais segurança ao idoso, fazendo com que qualquer tipo de imprevisto que possa vir a acontecer, o nosso paciente já esteja acompanhado de um profissional qualificado. Até mesmo para que os nosso profissionais possam auxiliar nossos pacientes no que for necessário, em suas atividades.</p>
<p>Sendo uma das principais empresas do segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado possui os melhores recursos do mercado com o objetivo de disponibilizar Alzheimer Cuidados de Enfermagem, Cuidador Hospitalar Preço, Cuidador de Idosos Hospitalar, Cuidador de Idosos 3 Vezes por Semana e Empresa de Cuidadores de Idosos Domiciliar com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Cuidar de Idosos a Noite em Porto da Igreja - Guarulhos com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>