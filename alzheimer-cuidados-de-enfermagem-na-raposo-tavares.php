<?php
$title       = "Alzheimer Cuidados de Enfermagem na Raposo Tavares";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>No serviço de Alzheimer Cuidados de Enfermagem na Raposo Tavares, da Onix, os profissionais são instruídos a realizarem atividades diárias com os nossos pacientes, dando a total e devida atenção aos mesmos, para que através de nossos trabalhos, suas qualidades de vida possam ser melhoradas. Contamos com enfermeiros e gerontólogos com anos de experiência no ramo e recursos que são adaptáveis a qualquer situação.</p>
<p>Na busca por uma empresa referência, quando o assunto é Cuidado ao Idoso, a Onix Gestão Do Cuidado será sempre a escolha que mais se destaca entre as principais concorrentes. Pois, além de fornecedor de Acompanhante de Idosos em Hospital, Cuidadora de Idosos Particular, Acompanhante de Idosos, Gerontologia Cuidado ao Idoso e Agência de Home Care Cuidador, oferece Alzheimer Cuidados de Enfermagem na Raposo Tavares com a melhor qualidade da região, também visa garantir o melhor custo x benefício, com agilidade e dedicação para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>