<ul>
    <li><a href="" title="Inicio">Inicio</a></li>
    <li><a href="" title="Serviços">Serviços</a></li>
        <ul>
            <li><a href="https://onixcuidadores.com.br/#servicess" title="Cuidadores de Idosos">Cuidadores de Idosos</a>
                <ul>
                    <li><a href="https://onixcuidadores.com.br/cuidadores-de-idosos/" title="Cuidadoes de Idosos">Cuidadoes de Idosos</a></li>
                    <li><a href="https://onixcuidadores.com.br/amigo-do-idoso/" title="Amigo dos Idosos">Amigo dos Idosos</a></li>
                    <li><a href="https://onixcuidadores.com.br/acompanhamento-hospitalar/" title="Acompanhante Hospitalar">Acompanhante Hospitalar</a></li>
                    <li><a href="https://onixcuidadores.com.br/procedimentos/" title="Procedimentos Pontuais e Específicos">Procedimentos Pontuais e Específicos</a></li>
                    <li><a href="https://onixcuidadores.com.br/agenciamento-de-cuidadores/" title="Agenciamento de Cuidadoes">Agenciamento de Cuidadoes</a></li>
                    <li><a href="https://onixcuidadores.com.br/visita-de-enfermagem/" title="Visita de Enfermagem">Visita de Enfermagem</a></li>
                </ul>
            </li>
            <li><a href="https://onixcuidadores.com.br/#servicess" title="Equipe Multiprofissional">Equipe Multiprofissional</a>
                <ul>
                    <li><a href="https://onixcuidadores.com.br/equipe-multiprofissional/" title="Equipe Multiprofissional">Equipe Multiprofissional</a></li>
                    <li><a href="https://onixcuidadores.com.br/gerenciamento-de-enfermagem/" title="Gerenciamento Enfermagem">Gerenciamento Enfermagem</a></li>
                    <li><a href="https://onixcuidadores.com.br/suporte-de-materiais-2/" title="Acompanhante Gerontológico">Acompanhante Gerontológico</a></li>
                    <li><a href="https://onixcuidadores.com.br/mente-ativa/" title="Mente Ativa">Mente Ativa</a></li>
                    <li><a href="https://onixcuidadores.com.br/avaliacao-da-casa-segura/" title="Avaliação da Casa Segura">Avaliação da Casa Segura</a></li>
                    <li><a href="https://onixcuidadores.com.br/suporte-de-materiais/" title="Suporte de Materiais e Equipamentos">Suporte de Materiais e Equipamentos</a></li>
                    <li><a href="https://onixcuidadores.com.br/de-volta-ao-lar/" title="De Volta ao Lar">De Volta ao Lar</a></li>
                    <li><a href="https://onixcuidadores.com.br/treinamento-do-cuidador/" title="Treinamento do Cuidador Familiar e/ou Particular">Treinamento do Cuidador Familiar e/ou Particular</a></li>
                    <li><a href="https://onixcuidadores.com.br/seu-transporte-seguro/" title="Seu Transporte Seguro">Seu Transporte Seguro</a></li>
                </ul>
            </li>
        </ul>
    <li>
        <a href="https://onixcuidadores.com.br/duvidas-frequentes/" title="Dúvidas Frequentes">Dúvidas Frequentes</a>
    </li>
    <li><a href="https://onixcuidadores.com.br/#conteudo" title="Conteúdo">Conteúdo</a>
        <ul>
            <li><a href="https://onixcuidadores.com.br/wp-content/uploads/2021/12/SOCIALIZACAO-DOS-IDOSOS.pdf" title="Socialização dos Idosos">Socialização dos Idosos</a></li>
            <li><a href="https://onixcuidadores.com.br/wp-content/uploads/2021/10/Queda-na-terceira-idade.pdf" title="Queda na Terceira Idade">Queda na Terceira Idade</a></li>
            <li><a href="https://onixcuidadores.com.br/#" title="Revista Onix">Revista Onix</a>
                <ul class="sub-menu-menu">
                    <li><a href="https://pt.calameo.com/read/00678768235b792bfa336" title="1° Edição">1° Edição</a></li>
                    <li><a href="https://pt.calameo.com/read/006787682f7be05180003" title="2° Edição">2° Edição</a></li>
                </ul>
            </li>
            <li><a href="https://onixcuidadores.com.br/blog/" title="Blog">Blog</a></li>
            <li><a href="https://onixcuidadores.com.br/videos/" title="Videos">Videos</a></li>
        </ul>
    </li>
    <li><a href="https://onixcuidadores.com.br" title="Contato">Contato</a>
        <ul class="sub-menu-2">
            <li><a href="https://onixcuidadores.com.br/campanha-de-junho/" title="Solicite Orçamento">Solicite Orçamento</a></li>
            <li><a href="https://onixcuidadores.com.br/trabalhe-conosco/" title="Trabalhe Conosco">Trabalhe Conosco</a></li>
        </ul>
    </li>
    <li><a href="index.php" title="Informações">Informações</a>
        <ul>
            <?php echo $padrao->subMenu($palavras_chave); ?>
        </ul>
    </li>
</ul>