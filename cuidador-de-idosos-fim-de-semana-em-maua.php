<?php
$title       = "Cuidador de Idosos Fim de Semana em Mauá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nosso serviço de Cuidador de Idosos Fim de Semana em Mauá, estão presentes profissionais gerontólogos e enfermeiros, que possuem anos de experiência nesse ramo, para que estejam preparados e aptos a qualquer situação que chegar até os mesmos. Portanto, não deixe de nos consultar para conhecer de perto a excelência de nossos trabalhos. Será um prazer levarmos nossos serviços a você.</p>
<p>Você procura por Cuidador de Idosos Fim de Semana em Mauá? Contar com empresas especializadas no segmento de Cuidado ao Idoso é sempre a melhor saída, já que assim temos a segurança de excelência e profissionalismo. Pensando assim, a empresa Onix Gestão Do Cuidado é a opção certa para quem busca a soma de qualidade, comprometimento e agilidade em Avaliação Gerontológica Preço, Acompanhante de Idosos, Empresa de Acompanhante de Idosos, Acompanhamento Hospitalar para Idoso e Cuidador de Idosos em Casa e ainda, um atendimento personalizado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>