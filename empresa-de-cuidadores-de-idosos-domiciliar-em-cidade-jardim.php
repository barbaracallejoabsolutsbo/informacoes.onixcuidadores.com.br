<?php
$title       = "Empresa de Cuidadores de Idosos Domiciliar em Cidade Jardim";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A nossa Empresa de Cuidadores de Idosos Domiciliar em Cidade Jardim fornece um acompanhamento integral ou suporte 24h, para nos moldarmos a rotina de nossos pacientes e também para que os mesmos possam nos consultar sem ter preocupações. Além de atendermos a domicílios, atendemos em diversos ambientes que seja de acordo com a realidade de nossos pacientes.</p>
<p>Conseguindo ofertar Treinamentos para Cuidadores de Idosos, Cuidador de Idosos 3 Vezes por Semana, Cuidadora de Idosos Preço, Empresa de Acompanhante de Idosos e Gerontologia Cuidado ao Idoso a Onix Gestão Do Cuidado proporciona Empresa de Cuidadores de Idosos Domiciliar em Cidade Jardim ideal para atender às reais necessidades de seus clientes e parceiros, assim comprovando o fato de ser a empresa mais completa e eficiente do mercado de Cuidado ao Idoso. Unindo a experiência que nossa empresa traz com profissionais competentes conseguimos proporcionar o melhor para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>