<?php
    $title       = "Avaliação gerontológica";
    $description = "Faça já a sua avaliação gerontológica com os profissionais da Onix. Estamos ansiosos para te apresentarmos nossos serviços e melhorar sua qualidade de vida. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p><br />Para você que busca por uma avaliação gerontológica de qualidade, que corresponderá suas necessidades, alcançado seus objetivos e com um baixo custo, a Onix é o lugar ideal. Agende o quanto antes uma avaliação gerontológica, através de nossos sites. Nós estamos localizados em São Paulo para que você possa vir até nós. Mas vale lembrar, que fornecemos nossos serviços em domicílios, clínicas, hospitais e casas de repousos. Portanto, consulte nossos profissionais para vermos qual opção é mais viável para você. Nós estamos há longos anos atuando nesse ramo, com profissionais extremamente qualificados, sendo eles gerontólogos e enfermeiros e os mesmo possuem grandes experiências com avaliação gerontológica e demais serviços de cuidados para idosos. Em nossa avaliação gerontológica, são realizadas diversas atividades e relatórios, para que possamos acompanhar a evolução de cada paciente, com os nossos trabalhos. E até mesmo para que os familiares e responsáveis acompanhem a avaliação da melhor forma também. Um dos nossos objetivos é fazermos com que nossos pacientes sintam o máximo de conforto ao consultarem nossos trabalhos e para isso, fazemos o que for necessário para garantirmos tal feito. Como por exemplo, nós disponibilizamos a alteração de funcionário, caso o nosso paciente não se adapte ao mesmo e diversos outros fatores. Estamos sempre estudando técnicas das mais atualizadas e os melhores recursos, para que possamos não só alcançar as expectativas de nossos clientes para conosco, mas para as superarmos. Desde o seu primeiro contato conosco, nós entregamos nossos melhores serviços e atendimentos para que a qualquer momento em que você necessitar desse ou demais serviços, nós sejamos a sua primeira opção.</p>

<h2>Mais detalhes sobre nossa avaliação gerontológica</h2>
<p>Através da avaliação gerontológica, você verá que nossos profissionais auxiliam nossos pacientes em cada atividade, para que os mesmos se tornem cada vez mais independentes por conta de suas evoluções. Um de nossos princípios é podermos fornecer uma melhoria de qualidade de vida aos nossos pacientes, contudo, desde seu mínimo contato conosco, você se torna nossa prioridade. Realize o seu orçamento em nosso site, consultando um de nossos profissionais através do mesmo. Mas caso prefira, nossos meios de contatos stão sempre disponíveis também.</p>

<h3>O melhor lugar para realizar uma avaliação gerontológica</h3>
<p>Para facilitar o seu acesso até nós e até mesmo para agendar uma avaliação gerontológica, nós possuímos nosso aplicativo próprio. Portanto, não adie mais essa oportunidade de poder usufruir de nossos serviços. Aguardamos ansiosamente pelo seu contato. Conte sempre conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>