<?php
$title       = "Treinamentos para Cuidadores de Idosos na Consolação";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Durante o nosso Treinamentos para Cuidadores de Idosos na Consolação, você verá como são atribuídos nossos trabalhos, juntamente com os nossos recursos de qualidade. Portanto, para que você tenha conhecimentos exclusivos e somente que a Onix pode te fornecer, entre em contato com um de nossos representantes. Será um prazer contribuirmos para o seu crescimento profissional.</p>
<p>Com credibilidade no mercado de Cuidado ao Idoso, a Onix Gestão Do Cuidado trabalha dia a dia com foco em proporcionar com qualidade, viabilidade e custo x benefício acessível tanto Cuidar de Idosos a Noite, Cuidador de Idosos Fim de Semana, Cuidador de Idosos para Dar Banho, Serviço de Cuidadores de Idosos e Avaliação Gerontológica Preço quanto em Treinamentos para Cuidadores de Idosos na Consolação. Por isso, se você busca pelo melhor para você, conte com a nossa equipe de profissionais altamente capacitados, garantindo assim seu sucesso no mercado.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>