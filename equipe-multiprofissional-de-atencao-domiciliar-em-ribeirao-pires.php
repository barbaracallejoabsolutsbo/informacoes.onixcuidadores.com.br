<?php
$title       = "Equipe Multiprofissional de Atenção Domiciliar em Ribeirão Pires";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Em nossa Equipe Multiprofissional de Atenção Domiciliar em Ribeirão Pires, contamos com profissionais gerontólogos, enfermeiros e demais da área da saúde, para que nossos pacientes tenham todo o respaldo necessário independente de suas necessidades. A nossa equipe e empresa está em constante evolução para que nos tornemos a cada dia, referência no ramo de cuidados para idosos.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Cuidado ao Idoso, a Onix Gestão Do Cuidado ganha destaque por ser confiável e idônea quando falamos não só de Equipe Multiprofissional de Atenção Domiciliar em Ribeirão Pires, mas também quando o assim é Quanto Custa Cuidadores de Idosos, Cuidador de Idosos com Alzheimer, Cuidar de Idosos a Noite, Home Care Cuidador de Idosos e Acompanhante de Idosos. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>