<?php
$title       = "Diária de um Cuidador de Idoso em Itapecerica da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Com a Diária de um Cuidador de Idoso em Itapecerica da Serras, o idoso terá todo o cuidado e auxílio necessário, com um acompanhamento integral ou suporte  24h. Portanto, para garantir uma melhoria na qualidade de vida de seu familiar, agenda uma avaliação com um de nossos especialistas através de nosso site, para que nossos profissionais lhe apresentem nossos melhores trabalhos.</p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de Cuidado ao Idoso, a Onix Gestão Do Cuidado é uma empresa experiente quando se trata do ramo de Agência de Home Care Cuidador, Serviço de Cuidadores de Idosos, Empresa de Cuidadores de Idosos Domiciliar, Cuidador de Idosos Hospitalar e Cuidador de Idosos 3 Vezes por Semana. Por isso, se você busca o melhor com um custo acessível e vantajoso em Diária de um Cuidador de Idoso em Itapecerica da Serra aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>