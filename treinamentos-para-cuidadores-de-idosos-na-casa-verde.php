<?php
$title       = "Treinamentos para Cuidadores de Idosos na Casa Verde";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Treinamentos para Cuidadores de Idosos na Casa Verde da Onix, é constituído por profissionais especializados na área, para que possamos formar pessoas cada vez mais calaceiradas para exercerem suas funções, colocando sempre o paciente como suas prioridades, atendendo sempre suas expectativas. Realize o seu treinamento conosco, para garantir mais experiência. </p>
<p>Buscando por uma empresa de credibilidade no segmento de Cuidado ao Idoso, para que, você que busca por Treinamentos para Cuidadores de Idosos na Casa Verde, tenha a garantia de qualidade e idoneidade, contar com a Onix Gestão Do Cuidado é a opção certa. Aqui tudo é feito por um time de profissionais com amplo conhecimento em Cuidar de Idosos em Casas Particulares, Acompanhamento Hospitalar para Idoso, Cuidar de Idosos Fim de Semana, Quanto Custa Cuidador 24 horas e Cuidador de Idosos em Casa para assim, oferecer a todos os clientes as melhores soluções.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>