<?php
    $title       = "Cuidador de idosos para dar banho";
    $description = "Nosso serviço de cuidador de idosos para dar banho é extremamente essencial para que o nosso paciente tenha toda a segurança necessária. Entre em contato com a Onix.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    

<p>É de conhecimento de todos saber do fato de que alguns idosos necessitam de mais atenção e cuidado do que outros. Pois os mesmos ficam extremamente vulneráveis para realizar em certos tipos de atividade até mesmo o banho. E por conta disso o serviço de cuidador de idosos para dar banho da onix, É extremamente necessário para que o idoso tenha alguém profissionalmente capacitado para realizar tal atividade com a segurança devida. O nosso serviço de cuidador de idosos para dar banho é fornecido para diversos ambientes como casas de repouso, hospitais, clínicas e até mesmo no próprio domicílio. Portanto consulte um de nossos profissionais para ver qual dessas opções é a mais viável a você. O cuidador de idosos para dar banho é essencial para você que por algum motivo pessoal não consegue depositar 100% de sua atenção nesse momento. E para isso os profissionais da ônix estão aqui para te auxiliar e ajudar no que for necessário até mesmo para melhorarmos a qualidade de vida do idoso. Ao navegar em nosso site você verá que nós possuímos diversos outros serviços para estimularmos cada vez mais a independência do idoso para com as suas atividades. Entre em contato o quanto antes com de nossos profissionais para nos apresentar a sua situação e então para que os mesmos possam fornecer O serviço que for necessário para você. Nós possuímos os melhores recursos para que possamos entregar cada vez mais um trabalho de qualidade através de nosso cuidador de idosos para dar banho. E até mesmo para que possamos nos tornar referência nesse ramo E então mais pessoas terem contato a um serviço de excelência.</p>

<h2>Mais detalhes sobre o nosso cuidador de idosos para dar banho</h2>
<p>Todos os profissionais da onyx são extremamente capacitados para exercer o serviço de cuidador de idosos para dar banho pois os mesmos são diplomatas e possui extremos conhecimentos absorvidos durante todos os seus anos atuando dentro dessa área. E os mesmos vem se qualificando cada vez mais para que estejam aptos a qualquer tipo de situação que chegar até eles.</p>

<h3>O melhor lugar para obter o serviço de cuidador de idosos para dar banho</h3>
<p>Queremos facilitar cada vez mais o contato de nossos clientes e pacientes conosco e para isso você pode falar com um de nossos especialistas sobre o nosso serviço de cuidador de idosos para dar banho através de nosso site.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>