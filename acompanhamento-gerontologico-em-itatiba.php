<?php
$title       = "Acompanhamento Gerontológico em Itatiba";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Onix é uma empresa especializada em serviços de Acompanhamento Gerontológico em Itatiba, pois contamos com profissionais com grandes experiências; longos anos de estudos e diplomatas, para que então, os mesmos estejam aptos para corresponderem a qualquer tipo de necessidade que chegar até nós. Lembrando que realizamos atividades e relatórios diários para fazer a análise da evolução de nossos pacientes. </p>
<p>Com a Onix Gestão Do Cuidado proporcionando o que se tem de melhor e mais moderno no segmento de Cuidado ao Idoso consegue garantir aos seus clientes a confiança e conforto que todos procuram. Com o melhor em Empresa de Cuidadores de Idosos Domiciliar, Acompanhamento Hospitalar para Idoso, Cuidador de Idosos com Fratura de Fêmur, Cuidar de Idosos a Noite e Agência de Home Care Cuidador nossa empresa, hoje, consegue possibilitar diversas escolhas para os melhores resultados, ganhando destaque e se tornando referência em Acompanhamento Gerontológico em Itatiba.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>