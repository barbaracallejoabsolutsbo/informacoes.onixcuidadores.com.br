<?php
$title       = "Cuidador de Idosos 3 Vezes por Semana na Vila Curuçá";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Durante o acompanhamento de Cuidador de Idosos 3 Vezes por Semana na Vila Curuçá nossos profissionais fornecem atividades aos nossos pacientes para que os mesmos possam ter constantes evoluções no que precisam e para realizarem atividades diárias sem nenhum impedimento. Nós moldamos nossos horários conforme a rotina de nossos pacientes, para que os mesmos possam ter acesso aos nossos serviços a qualquer momento.</p>
<p>Nós da Onix Gestão Do Cuidado trabalhamos dia a dia para garantir o melhor em Cuidador de Idosos 3 Vezes por Semana na Vila Curuçá e para isso contamos com profissionais altamente capacitados para atender e garantir a satisfação de seus clientes e parceiros. Atuando no mercado de Cuidado ao Idoso com qualidade e dedicação, contamos com profissionais com amplo conhecimento em Agência de Home Care Cuidador, Serviço de Cuidadores de Idosos, Quanto Custa Cuidadores de Idosos, Empresa de Cuidadores de Idosos e Cuidador de Idosos Fim de Semana e muito mais.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>