<?php
$title       = "Equipe Multiprofissional de Atenção Domiciliar em Torres Tibagy - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Vale lembrar que a nossa Equipe Multiprofissional de Atenção Domiciliar em Torres Tibagy - Guarulhos, atende a diversos ambientes além do domicílio. Como por exemplo, clínicas, casas de repousos e hospitais. Portanto, não pense que por conta da sua atual situação, os nossos serviços são inalcançáveis a você. Temos disponibilidades de horários, conforme a rotina do clientes para que 
 possam nos solicitar a qualquer momento.</p>
<p>A Onix Gestão Do Cuidado, como uma empresa em constante desenvolvimento quando se trata do mercado de Cuidado ao Idoso visa trazer o melhor resultado em Equipe Multiprofissional de Atenção Domiciliar em Torres Tibagy - Guarulhos para todos os clientes que buscam uma empresa de confiança e competência. Contamos com profissionais com amplo conhecimento em Cuidar de Idosos a Noite, Empresa de Acompanhante de Idosos, Acompanhante de Idosos em Hospital, Acompanhamento Hospitalar e Cuidador de Idosos para Dar Banho para levar sempre o melhor para você, garantindo assim a sua satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>