<?php
$title       = "Cuidar de Idosos em Casas Particulares em São Caetano do Sul";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossos profissionais gerontólogos e enfermeiros prestam seus serviços de Cuidar de Idosos em Casas Particulares em São Caetano do Sul, clínicas, hospitais, casas de repousos e demais ambientes. Portanto, nos apresente suas necessidades para que possamos lhe fornecer nossa melhor opção de serviço. Fazemos o que for necessário para que melhoremos a qualidade de vida de cada paciente.</p>
<p>Especialista no mercado, a Onix Gestão Do Cuidado é uma empresa que ganha visibilidade quando se trata de Cuidar de Idosos em Casas Particulares em São Caetano do Sul, já que possui mão de obra especializada em Cuidar de Idosos em Casas Particulares, Alzheimer Cuidados de Enfermagem, Acompanhante de Idosos em Hospital, Cuidar de Idosos a Noite e Acompanhamento Gerontológico. Nossa empresa vem crescendo e garantindo seu espaço entre as principais empresas do ramo de Cuidado ao Idoso, onde tem o foco em trazer o que se tem de melhor para seus clientes.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>