<?php
$title       = "Acompanhamento de Idosos em Hospitais em São Paulo";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Nossa empresa é constituída por diversos profissionais da saúde, como gerontólogos e enfermeiros, para que os nossos pacientes tenham o atendimento correto e profissional durante o nosso Acompanhamento de Idosos em Hospitais em São Paulo. Nós entregamos relatórios diários, para que a família e responsáveis possam ter ciência de como os nossos serviços estão sendo aplicados.</p>
<p>A empresa Onix Gestão Do Cuidado é destaque entre as principais empresas do ramo de Cuidado ao Idoso, vem trabalhando com o princípio de oferecer aos seus clientes e parceiros o melhor em Acompanhamento de Idosos em Hospitais em São Paulo do mercado. Ainda, possui facilidade com Cuidador de Idosos Fim de Semana, Acompanhamento Gerontológico, Empresa Terceirizada de Cuidadores de Idosos, Cuidadora de Idosos Particular e Onde Encontrar Cuidadores de Idosos mantendo a mesma excelência. Pois, contamos com a melhor equipe da área em que atuamos a diversos anos.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>