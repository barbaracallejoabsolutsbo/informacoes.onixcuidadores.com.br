<?php
$title       = "Cuidadora de Idosos Particular em Osasco";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>A Cuidadora de Idosos Particular em Osasco da Onix, atende em qualquer ambiente que você nos solicitar, sejam eles casas de repouso, hospitais, domicílios ou clínicas. Nós contamos com profissionais gerontólogos e enfermeiros extremamente qualificados, para que possam exercer suas funções com grande êxito, garantindo a satisfação de nossos pacientes. Não deixe de nos consultar para saber mais.</p>
<p>Sendo uma das empresas mais confiáveis no ramo de Cuidado ao Idoso, a Onix Gestão Do Cuidado ganha destaque por ser confiável e idônea quando falamos não só de Cuidadora de Idosos Particular em Osasco, mas também quando o assim é Serviço de Acompanhamento de Idosos, Atenção Domiciliar ao Idoso, Diária de um Cuidador de Idoso, Cuidador de Idosos para Dar Banho e Cuidador de Idosos com Alzheimer. Pois, aqui tudo é realizado por um time de profissionais experientes e que trabalham para oferecer a todos os clientes as melhores soluções para você.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>