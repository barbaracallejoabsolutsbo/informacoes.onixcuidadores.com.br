<?php
    $title       = "Empresa de cuidadores de idosos domiciliar";
    $description = "A Onix, é uma empresa de cuidadores de idosos domiciliar. Portanto, independente de quando você precisar de serviços para idosos, conte conosco!";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <div class="titulo-personalizado"><div class="container"><div class="col-md-8"><h1 class="main-title"><?php echo $h1; ?></h1></div><div class="col-md-4"><?php echo $padrao->breadcrumb(array("Informações", $title)); ?></div></div></div><section class="container">
            
            
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php // echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p> A nossa empresa de cuidadores de idosos domiciliar, realiza atividades diárias, através de nossos profissionais, onde os mesmos auxiliam os nossos pacinetes, a evoluírem no que for necessário, para que garantam cada vez mais suas independências ao realizarem seus afazeres diários. A Onix, empresa de cuidadores de idosos domiciliar, conta com profissionais gerontólogos, enfermeiros e demais outros da ártea da saúde, para que tenhamos cada vez mais um serviço completo e que corresponda a todas as necessidades de nossos pacinetes. Nós estamos sempre estudando os métodos mais atualizados e novas técinicas que possam ser utilizados nos serviços de nossa empresa de cuidadores de idosos domiciliar, para que nos tornemos cada vez mais referência nesse ramo. Até mesmo para que sempre que você precisar de um de nossos serviços prestados, você já tenha ciência que somos o melhor lugar para se recorrer. A nossa empresa de cuidadores de idosos domiciliar, fornece nossos serviços para ambientes além do domicílio, como por exemplo, hospitais, clínicas e casas de repousos. Portanto, consulte um de nossos profissionais, para garantir a opção que for mais viável a você. Durante cada acompanhamento, nossos profissionais entregam relatórios, para que a família e responsáveis possam ter mais detalhes de comos os nossos serviços estão sendo apliacados. Além do nosso objetivo de melhorar qualidade de vida, nós desejamos que os nossos pacientes sinatem o máximo de conforto ao presenciarem qualuqer um de nossos serviços. E para isso, nós disponibilizamos a troca de funcionário, caso haja uma falta de adaptação dos nossos pacientes para com os mesmos. E até mesmo a troca de horário de atendimento, para nos adaprtarmos a rotina de nossos paciente. </p>

<p>Mais informações sobre nossa empresa de cuidadores de idosos domiciliar </p>
<p>A nossa empresa de cuidadores de idosos domiciliar, deseja levar nossos serviços para mais pessoas, a cada dia mais. E para isso, desde o nosso princípio, mantemos nosso preço baixo, porém com uma alta qualidade. Em nosso site, você pode realizar o seu orçamento a qualquer hora, para garantir o quanto antes qualquer um dos serviços que fornecemos. </p>
<p>A melhor empresa de cuidadores de idosos domiciliar </p>
<p>A nossa empresa de cuidadores de idosos domiciliar, está localizada em São Paulo para você que nos deseja fazer uma visista, mas em nosso site, há diversos meios de contatos, onde você pode falar diretamente com um de nossos especialistas. Ou até mesmo através de nosso aplicativo, para agendar uma avaliação.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>