<?php
$title       = "Cuidador de Idosos para Dar Banho em Jandira";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Além do serviço de Cuidador de Idosos para Dar Banho em Jandira, nós fornecemos serviços similares aos nossos clientes, para que possamos auxilia-los no que for necessário. Nós fornecemos nossos serviços em ambientes como, domicílio, hospitais, casa de repouso e clínicas. Portanto, nos apresente a opção que for mais viável a você, para que possamos te entregar nossos melhores trabalhos.</p>
<p>Se você está em busca de Cuidador de Idosos para Dar Banho em Jandira tem preferência por uma empresa com conhecimento e custo-benefício, a Onix Gestão Do Cuidado é a melhor opção para você. Com uma equipe formada por profissionais amplamente qualificados e dedicados para oferecer soluções personalizadas para cada cliente que busca pela excelência. Entre em contato com a gente e saiba mais sobre Cuidado ao Idoso e todos os nossos serviços como Cuidadora de Idosos Preço, Cuidar de Idosos a Noite, Agência de Home Care Cuidador, Empresa de Acompanhante de Idosos e Cuidadora de Idosos Particular.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>