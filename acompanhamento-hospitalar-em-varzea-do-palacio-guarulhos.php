<?php
$title       = "Acompanhamento Hospitalar em Várzea do Palácio - Guarulhos";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O Acompanhamento Hospitalar em Várzea do Palácio - Guarulhos da Onix, pode ser feito de forma integral ou com suporte 24h, portanto, consulte um de nossos profissionais através de nosso site ou pelos nossos números telefônicos. Navegue em nosso site para conhecer mais detalhadamente sobre esse e demais serviços que possuímos em nossa empresa. Nosso maior objetivo é melhorarmos qualidade de vidas, através de nossos trabalhos.</p>
<p>Desempenhando uma das melhores assessorias do segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado se destaca no mercado, uma vez que, conta com os melhores recursos da atualidade, de modo a fornecer Acompanhamento Hospitalar em Várzea do Palácio - Guarulhos com eficiência e qualidade. Venha e faça uma cotação com um de nossos atendentes especializados em Cuidador de Idosos 3 Vezes por Semana, Gerontologia Cuidado ao Idoso, Empresa de Cuidadores de Idosos, Cuidador de Idosos com Fratura de Fêmur e Cuidador de Idosos com Alzheimer, pois somos uma empresa especializada.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>