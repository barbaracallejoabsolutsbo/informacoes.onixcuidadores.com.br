<?php
$title       = "Acompanhamento de Idosos em Hospitais em Taboão da Serra";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>O serviço de Acompanhamento de Idosos em Hospitais em Taboão da Serra, da Onix, é uma ótima opção a você que pela correria do dia a dia ou outros fatores, não consegue entregar sua atenção o tempo todo ao idoso. Portanto, estamos aqui para te fornecer todo o suporte necessário, com profissionais qualificados e capacitados, onde você não terá receio nenhum ao depositar sua confiança. </p>
<p>Atuando de forma a cumprir os padrões de qualidade do mercado de Cuidado ao Idoso, a Onix Gestão Do Cuidado é uma empresa experiente quando se trata do ramo de Cuidadora de Idosos Particular, Empresa de Cuidadores de Idosos, Empresa de Cuidador de Idosos, Quanto Custa Cuidador 24 horas e Equipe Multiprofissional de Atenção Domiciliar. Por isso, se você busca o melhor com um custo acessível e vantajoso em Acompanhamento de Idosos em Hospitais em Taboão da Serra aqui você encontra o que precisa sem a diminuição da qualidade. Busque sempre o melhor para ter uma real satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>