<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <div class="logo text-center">
            <img src="imagens/logo-footer.png" alt="logo-footer" title="logo footer">
            <br>
            <a href="https://www.facebook.com/onixcuidadoresdeidosos" title="Facebook"><i class="fab fa-facebook"></i></a><a href="mailto:contato@onixgestao.com.br" title="email"><i class="fas fa-envelope"></i></a><a href="https://www.instagram.com/onixcuidadoresdeidosos/" title="instagram"><i class="fab fa-instagram"></i></a><a href="https://www.linkedin.com/company/onix-gestão-do-cuidado-ao-idoso/" title="linkedin"><i class="fab fa-linkedin"></i></a>
        </div>
        <div class="row">
            <div class="selo col-md-4">
                <h2>Qualidade ABG</h2>
                <img src="imagens/selo3.png" alt="selo3" title="Selo">
                </div>
                <div class="contato-footer col-md-4">
                    <h2 class="text-center">Contatos</h2>
                    <h4 class="text-center">Atendimento Onix Gestão</h4>
                    <p class="text-center">Seg - Dom</p>
                    <h4 class="text-center">Atendimento 24 horas</h4>
                    <p class="text-center"><b>(11) 3214-1652</b></p>
                    <p class="text-center"><b>(11) 93027-0237</b></p>
                    <p class="text-center"><b>(11) 98430-0237</b></p>
                </div>
                <div class="col-md-4">
                    <h2 class="text-center">Localização</h2>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3656.7999785318534!2d-46.71244674953974!3d-23.575626267928875!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce56ffc037418b%3A0xa6490ffe37a3c781!2sR.%20Alvarenga%2C%20700%20-%20Butant%C3%A3%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2005509-000!5e0!3m2!1spt-BR!2sbr!4v1643401839749!5m2!1spt-BR!2sbr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    <p class="text-center">
                        <i class="fas fa-map-marker-alt"></i> <?php echo $unidades[1]["rua"] . " - " . $unidades[1]["bairro"] . " <br> " . $unidades[1]["cidade"] . " - " . $unidades[1]["uf"] . " - " . $unidades[1]["cep"]; ?>
                    </p>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                            <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="baixa-absolut"></div>
                        <p class="absolutsbo"><a href="https://www.absolutsbo.com.br/" target="_blank" title="Absolut SBO">Desenvolvido por <strong>Absolut SBO</strong></a></p>
                    </div>
                    <div class="col-md-4 text-right">
                        <a href="<?php echo $url; ?>mapa-site" title="Mapa do Site">
                            <img src="<?php echo $url; ?>assets/img/icons/sitemap.png" alt="Sitemap"></a>
                        <a rel="nofollow" href="http://validator.w3.org/check?uri=<?php echo $canonical; ?>" target="_blank" title="HTML 5 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-html5.png" alt="HTML 5 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://jigsaw.w3.org/css-validator/validator?uri=<?php echo $canonical; ?>" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/selo-css3.png" alt="CSS 3 - Site Desenvolvido nos padrões W3C">
                        </a>
                        <a rel="nofollow" href="http://www.absolutsbo.com.br/" target="_blank" title="CSS 3 - Site Desenvolvido nos padrões W3C">
                            <img src="<?php echo $url; ?>assets/img/icons/logo-footer.png" alt="Absolut SBO">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a href="whatsapp://send?text=<?php echo $canonical; ?>" class="mm-whatsapp" title="Whats App"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
    </footer>
    <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-PBWGN8BQL3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-PBWGN8BQL3');
</script>
        <?php } ?>