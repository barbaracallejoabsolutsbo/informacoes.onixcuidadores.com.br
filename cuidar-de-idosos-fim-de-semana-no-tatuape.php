<?php
$title       = "Cuidar de Idosos Fim de Semana no Tatuapé";
$description = "";
$h1          = $title;
$keywords    = $title;
$meta_img    = "";

include "includes/padrao/class.padrao.php";
include "includes/config.php";
include "includes/padrao/head.padrao.php";

$url_title   = $padrao->formatStringToURL($title);

$padrao->compressCSS(array(
    "tools/fancybox",
    "default_padrao/redes-sociais",
    "default_padrao/direitos-texto",
    "default_padrao/regioes",
    "default_padrao/veja-tambem",
    "palavra-chave"
));

?>
</head>
<body>

<?php include "includes/_header.php"; ?>

<main class="main-content">
    <section class="container">
        <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
        <h1 class="main-title"><?php echo $h1; ?></h1>
        <div class="row">
            <div class="col-md-9 text-justify">
                <img src="<?php echo $url."imagens/imagens-regionalizado/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right">
                <p>Estamos sempre estudando e utilizando de novas técnicas em nossos serviços prestados ao Cuidar de Idosos Fim de Semana no Tatuapé, para que nos tornemos cada vez mais referência nesse mercado, para que mais pessoas possam ter acesso aos nossos serviços de qualidade. Nosso maior objetivo é melhorarmos qualidades de vida de cada paciente que chegar até nós.</p>
<p>Sendo uma das principais empresas do segmento de Cuidado ao Idoso, a Onix Gestão Do Cuidado possui os melhores recursos do mercado com o objetivo de disponibilizar Equipe Multiprofissional de Atenção Domiciliar, Empresa de Cuidadores de Idosos, Cuidador Hospitalar Preço, Cuidador de Idosos 3 Vezes por Semana e Empresa Terceirizada de Cuidadores de Idosos com a qualidade que você merece. Por isso, entre em contato, faça um orçamento e conheça as nossas especialidades que vão além de Cuidar de Idosos Fim de Semana no Tatuapé com garantia de qualidade e satisfação.</p>
                <?php include "includes/social-media.php"; ?>
                <?php include "includes/regioes-sao-paulo.php"; ?>
                <?php // include "includes/regioes-brasil.php"; ?>
                <?php include "includes/direitos-texto.php"; ?>
            </div>
            <aside class="col-md-3">
                <?php include "includes/sidebar.php"; ?>
            </aside>
        </div>
        <?php include "includes/veja-tambem-regionalizado.php"; ?>
    </section>
</main>

<?php include "includes/_footer.php"; ?>

<?php $padrao->compressJS(array(
    "tools/jquery.fancybox",
    "tools/bootstrap.min",
    "tools/jquery.validate.min",
    "tools/jquery.mask.min",
    "jquery.quality.keyword"
)); ?>

</body>
</html>